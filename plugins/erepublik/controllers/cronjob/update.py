#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from xml.dom import minidom
from datetime import datetime
from urllib import quote

from plugins.erepublik.models import troops, companies
from application import gpress
from application.models import mailbox

from google.appengine.api import urlfetch, memcache
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Soldier=troops.Soldier
Squad=troops.Squad
Company=companies.Company
Message=mailbox.Message

SoldierURL = "http://api.erepublik.com/v1/feeds/citizens/%s?by_username=true" # soldierNAME
SoldierIdURL = "http://api.erepublik.com/v1/feeds/citizens/%s" # soldierID
CompanyIdURL = "http://api.erepublik.com/v1/feeds/companies/%s" # companyID
CountriesURL = "http://api.erepublik.com/v1/feeds/countries"
CountryIdURL = "http://api.erepublik.com/v1/feeds/countries/%s" # countryID
RegionIdURL = "http://api.erepublik.com/v1/feeds/regions/%s" # regionID
MarketURL = "http://api.erepublik.com/v1/feeds/market/%s/%s/%s" # industry, quality, country 
Exchange = "http://api.erepublik.com/v1/feeds/exchange/%s/%s" # buy, sell

class Soldiers(webapp.RequestHandler):
  def get_data(self):
    data = Soldier.all().fetch(1000)
    return data
    data = memcache.get("erepublik_soldiers")
    if data is not None:
      return data
    else:
      data = Soldier.all().fetch(1000)
      memcache.add("erepublik_soldiers", data, 86400)
      return data

  def get(self):
    Debug = False if self.request.headers.has_key("X-AppEngine-Cron") else True
    soldiers = self.get_data()
    fields = soldiers[0].properties().keys()
    failed = []
    
    for soldier in soldiers:
      if Debug: self.response.out.write("<hr /><br />Processing the soldier: <b>%s</b><br />"%soldier.name)
      try:
        result = urlfetch.fetch(SoldierURL%quote(soldier.name))
        if result.status_code == 200:
          if Debug: self.response.out.write("Feed fetched<br />")
          xml = minidom.parseString(result.content)
          if xml:
            if Debug: self.response.out.write("XML Parsed<br />")

            soldier.citizenship_country = xml.firstChild.getElementsByTagName('citizenship')[0].childNodes[1].childNodes[0].nodeValue
            soldier.citizenship_c_id = int(xml.firstChild.getElementsByTagName('c-id')[0].childNodes[0].nodeValue)
            for element in xml.firstChild.childNodes:
              nodename = str(element.nodeName).replace('-','_')
              if nodename == 'damage' and soldier.damage_when_joined is None:
                soldier.damage_when_joined = float(xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes[0].nodeValue)
              if nodename in fields:
                if Debug: self.response.out.write("Field %s"%nodename)
                value = xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes[0].nodeValue
                if Debug: self.response.out.write(" Value %s<br />"%str(value))
                if nodename == 'country':
                  value = xml.firstChild.getElementsByTagName(element.nodeName)[1].childNodes[0].nodeValue
                  if Debug: self.response.out.write("Fixed Value %s<br />"%str(value))
                  soldier.__setattr__(nodename, value)
                elif nodename in troops.soldier_integers:
                  soldier.__setattr__(nodename, int(value))
                elif nodename in troops.soldier_floaters:
                  soldier.__setattr__(nodename, float(value))
                elif nodename in troops.soldier_booleans:
                  soldier.__setattr__(nodename, True if value.lower() == 'true' else False)
                elif nodename in troops.soldier_lists:
                  new_list = ["%s|%s"%(x.childNodes[1].childNodes[0].nodeValue,x.childNodes[3].childNodes[0].nodeValue) if x.nodeName in ['medal','skill'] else None for x in xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes]
                  while new_list.count(None):
                    new_list.remove(None)
                  soldier.__setattr__(nodename, new_list)
                elif nodename in troops.soldier_datetimes:
                  date = str(value).split(' ')
                  year, month, day = date[0].split('-')
                  hours, minutes, seconds = date[1].split(':')
                  soldier.__setattr__(nodename, datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0])))
                elif nodename:
                  soldier.__setattr__(nodename, str(value))
            
            soldier.save()
            if Debug:
              self.response.out.write("Data Saved<br />" if soldier.is_saved() else "Failed!<br />")

        else: raise Exception
      except Exception, e:
        if Debug: self.response.out.write("PUPPA [%s]"%e.message)
        failed.append(soldier)

    if failed.__len__() > 0:
      Message(
              name="eRepublik Tracker Error",
              reference="contact the administrator",
              body="The data importer has failed for the following entries: %s" %"\n".join([str(s.id) if s.id else str(s.name) for s in failed])
              )

class Companies(webapp.RequestHandler):
  def clean_skills(self, skills):
    employer_skills = ["%s,%s" %(x.childNodes[1].childNodes[0].nodeValue,x.childNodes[3].childNodes[0].nodeValue) if x.nodeName in ['skill'] else None for x in  skills.childNodes]
    while employer_skills.count(None):
      employer_skills.remove(None)
    return "#".join(employer_skills)
    
  def get_data(self):
    data = memcache.get("erepublik_companies")
    if data is not None:
      return data
    else:
      data = Company.all().fetch(1000)
      memcache.add("erepublik_companies", data, 86400)
      return data

  def get(self):
    Debug = False if self.request.headers.has_key("X-AppEngine-Cron") else True
    companiez = self.get_data()
    fields = companiez[0].properties().keys()
    failed = []
    
    for company in companiez:
      if Debug: self.response.out.write("<hr /><br />Processing the company: <b>%s</b><br />"%company.name)
      try:
        result = urlfetch.fetch(CompanyIdURL%company.id)
        if result.status_code == 200:
          if Debug: self.response.out.write("Feed fetched<br />")
          xml = minidom.parseString(result.content)
          if xml:
            if Debug: self.response.out.write("XML Parsed<br />")
            for element in xml.firstChild.childNodes:
              nodename = str(element.nodeName).replace('-','_')
              if nodename in fields:
                if Debug: self.response.out.write("Field %s"%nodename)
                value = xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes[0].nodeValue
                if Debug: self.response.out.write(" Value %s<br />"%str(value))
                if nodename in companies.company_integers:
                  company.__setattr__(nodename, int(value))
                elif nodename in companies.company_floaters:
                  company.__setattr__(nodename, float(value))
                elif nodename in companies.company_booleans:
                  company.__setattr__(nodename, True if value.lower() == 'true' else False)
                elif nodename in companies.company_lists:
                  if nodename == 'employees':
                    new_list = ["%s|%s|%s|%s"%(x.getElementsByTagName('country')[0].childNodes[0].nodeValue,x.getElementsByTagName('citizen-id')[0].childNodes[0].nodeValue,x.getElementsByTagName('citizen-name')[0].childNodes[0].nodeValue,self.clean_skills(x.getElementsByTagName('skills')[0])) if x.nodeName in ['employee'] else None for x in xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes]
                  else:
                    new_list = ["%s|%s|%s"%(x.childNodes[1].childNodes[0].nodeValue,x.childNodes[3].childNodes[0].nodeValue,x.childNodes[5].childNodes[0].nodeValue) if x.nodeName in ['export-licence','job-offer'] else None for x in xml.firstChild.getElementsByTagName(element.nodeName)[0].childNodes]
                  while new_list.count(None):
                    new_list.remove(None)
                  company.__setattr__(nodename, new_list)
                elif nodename in companies.company_datetimes:
                  date = str(value).split(' ')
                  year, month, day = date[0].split('-')
                  hours, minutes, seconds = date[1].split(':')
                  company.__setattr__(nodename, datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0])))
                elif nodename:
                  company.__setattr__(nodename, str(value))
            
            company.save()
            if Debug:
              self.response.out.write("Data Saved<br />" if company.is_saved() else "Failed!<br />")

        else: raise Exception
      except Exception, e:
        if Debug: self.response.out.write("PUPPA [%s]"%e.message)
        failed.append(company)

    if failed.__len__() > 0:
      Message(
              name="eRepublik Tracker Error",
              reference="contact the administrator",
              body="The data importer has failed for the following entries: %s" %"\n".join([str(s.id) if s.id else str(s.name) for s in failed])
              )

    #check for raw materials
    #for company in companies:


application = webapp.WSGIApplication([('/plugins/erepublik/cronjob/update/soldiers', Soldiers),
                                                          ('/plugins/erepublik/cronjob/update/companies', Companies)],
                                                           gpress.Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()