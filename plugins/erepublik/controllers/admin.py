#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

from urllib import urlencode, unquote_plus as unquote
from plugins.erepublik.models import troops, companies

Soldier = troops.Soldier
Squad = troops.Squad
Company = companies.Company

class List(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.CONTRIBUTOR)
    tools.render(self, gp, 'table', {'soldiers': Soldier.all().fetch(1000),'squads': Squad.all().fetch(1000),'companies': Company.all().fetch(1000)})

class Write(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    
    tools.render(self, gp, 'soldierform', {'squads': Squad.all().fetch(1000)})
    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update soldier details"
      """ Update entry """
      try:
        soldier = db.get(db.Key(self.request.get('key')))
        if self.request.get('picture',default_value=False):
          soldier.picture = db.Key(self.request.get('picture'))
        else: soldier.picture = None
        soldier.update(self.request.POST)
        soldier.assigned_squad = self.request.get('assigned_squad', default_value=None)
        soldier.assigned_company = self.request.get('assigned_company', default_value=None)
        soldier.save()
      except Exception, e:
        gp.error = e
        gp.actionstatus = -1
    else:
      gp.page.title = "Add a soldier"
      soldier = Soldier(
            key_name=str(self.request.get('name')).lower(),
            name=self.request.get('name')
      )
      soldier.put()
      self.redirect("/gp-admin/plugins/erepublik/%s" % soldier.key(), False)
      return
    
    tools.render(self, gp, 'soldier', {'soldier': soldier, 'squads': Squad.all().fetch(1000)})

class Read(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    
    try:
      key = self.request.get('key',default_value=tools.get_url_var('erepublik'))
      soldier = Soldier.get(db.Key(key))
      tools.render(self, gp, 'soldier', {'soldier': soldier,'squads': Squad.all().fetch(1000),'companies': Company.all().fetch(1000)})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return
    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update soldier details"
      """ Update entry """
      soldier = Soldier.get(db.Key(self.request.get('key')))
      if self.request.get('picture',default_value=False):
        soldier.picture = db.Key(self.request.get('picture'))
      else: soldier.picture = None
      soldier.update(self.request.POST)
      soldier.assigned_squad = self.request.get('assigned_squad', default_value=None)
      soldier.assigned_company = self.request.get('assigned_company', default_value=None)
      soldier.save()

    tools.render(self, gp, 'soldier', {'soldier': soldier,'squads': Squad.all().fetch(1000),'companies': Company.all().fetch(1000)})
      
class Delete(webapp.RequestHandler):
  def get(self):
    gpress.Registry(self, gpress.USER)
    keys = tools.get_requested_object_list(3)
    
    while '' in keys:
      keys.remove('')
    if 'delete' in keys:
      keys.remove('delete')
    if 'all_entities' in keys:
      keys.remove('all_entities')
    db.delete(keys)
    
    self.redirect(os.environ["HTTP_REFERER"], False)

class SquadWrite(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    
    tools.render(self, gp, 'squadform', {})
    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update squad details"
      """ Update entry """
      try:
        squad = db.get(db.Key(self.request.get('key')))
        squad.name = self.request.get('name')
        squad.notes = self.request.get('notes',default_value="")
        if self.request.get('picture',default_value=False):
          squad.picture = db.Key(self.request.get('picture'))
        else: squad.picture = None
        squad.save()
      except Exception, e:
        gp.error = e
        gp.actionstatus = -1
    else:
      gp.page.title = "Add a squad"
      squad = Squad(
            name=self.request.get('name')
      )
      squad.put()
      self.redirect("/gp-admin/plugins/erepublik/squads-%s" %squad.key() , False)
      return
    
    tools.render(self, gp, 'squadform', {'squad': squad})


class SquadRead(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    key = tools.get_url_var('erepublik')
    try:
      squad = db.get(db.Key(unquote(key[7:])))
      tools.render(self, gp, 'squad', {'squad': squad})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update soldier details"
      """ Update entry """
      try:
        squad = db.get(db.Key(self.request.get('key')))
        squad.name = self.request.get('name')
        squad.notes = self.request.get('notes',default_value="")
        if self.request.get('picture',default_value=False):
          squad.picture = db.Key(self.request.get('picture'))
        else: squad.picture = None
        squad.save()
      except Exception, e:
        gp.error = e
        gp.actionstatus = -1

    tools.render(self, gp, 'squad', {'squad': squad})

class CompanyWrite(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    
    tools.render(self, gp, 'companyform', {})
    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update company details"
      """ Update entry """
      try:
        company = db.get(db.Key(self.request.get('key')))
        company.key_name=self.request.get('name')
        company.name = self.request.get('name')
        company.notes = self.request.get('notes',default_value="")
        if self.request.get('picture',default_value=False):
          company.picture = db.Key(self.request.get('picture'))
        else: company.picture = None
        company.save()
      except Exception, e:
        gp.error = e
        gp.actionstatus = -1
    else:
      gp.page.title = "Add a company"
      company = Company(
            id=int(self.request.get('id'))
      )
      company.put()
      self.redirect("/gp-admin/plugins/erepublik/companies-%s" %company.key() , False)
      return
    
    tools.render(self, gp, 'companyform', {'company': company})


class CompanyRead(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    key = tools.get_url_var('erepublik')
    try:
      company = db.get(db.Key(unquote(key[10:])))
      tools.render(self, gp, 'company', {'company': company})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

    
  def post(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    gp.actionstatus = True

    if self.request.get('key', default_value=False):
      gp.page.title = "Update company details"
      """ Update entry """
      try:
        company = db.get(db.Key(self.request.get('key')))
        company.notes = self.request.get('notes',default_value="")
        company.id = int(self.request.get('id', default_value=0))
        if self.request.get('picture',default_value=False):
          company.picture = db.Key(self.request.get('picture'))
        else: company.picture = None
        company.save()
      except Exception, e:
        gp.error = e
        gp.actionstatus = -1

    tools.render(self, gp, 'company', {'company': company})

Default = List
RoutingTable = [('/gp-admin/plugins/erepublik/soldiers-write', Write),
                        ('/gp-admin/plugins/erepublik/squads-write', SquadWrite),
                        ('/gp-admin/plugins/erepublik/companies-write', CompanyWrite),
                        ('/gp-admin/plugins/erepublik/list', List),
                        ('/gp-admin/plugins/erepublik/.*/delete', Delete),
                        ('/gp-admin/plugins/erepublik/', Default),
                        ('/gp-admin/plugins/erepublik/companies-.*', CompanyRead),
                        ('/gp-admin/plugins/erepublik/squads-.*', SquadRead),
                        ('/gp-admin/plugins/erepublik/.*', Read),
                        ('/gp-admin/plugins/erepublik.*', Default)]

application = webapp.WSGIApplication(RoutingTable, gpress.Debug)



def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()