#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.gpress import tools
from application.models import media
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app
from urllib import urlencode, unquote_plus as unquote

from plugins.erepublik.models import troops, companies

Soldier = troops.Soldier
Squad = troops.Squad
Company = companies.Company
Media = media.Media

class Soldiers(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    try:
     tools.render(self, gp, 'soldiers', {'soldiers': Soldier.all().order('-assigned_squad').fetch(1000)})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

class SoldiersLadder(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    try:
     tools.render(self, gp, 'soldiersladder', {'soldiers': Soldier.all().fetch(1000)})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

class SquadDetails(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    key = str(tools.get_url_var('erepublik'))
    try:
     tools.render(self, gp, 'squad', {'squad': Squad.get(db.Key(str(key[6:])))})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

class SoldierDetails(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    tools.parseHandler(self, gp)
    key = str(tools.get_url_var('erepublik'))

    try:
      soldier = Soldier.get_by_key_name(key[8:].lower())
      tools.render(self, gp, 'soldier', {'soldier': soldier})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

class Companies(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    tools.render(self, gp, 'companies', {'companies': Company.all().fetch(1000)})

class CompanyDetails(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    tools.parseHandler(self, gp)
    key = unquote(tools.get_url_var('erepublik'))
    try:
      soldiers = Soldier.all().fetch(1000)
      company = Company.all().filter('name =', str(key[8:])).fetch(1)[0]
      tools.render(self, gp, 'company', {'company': company, 'soldiers_list': [str(x.name).strip().lower() for x in soldiers]})
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

class GetSquadPicture(webapp.RequestHandler):
  def get_data(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    try:
      return Squad.get(db.Key(self.request.get('k', default_value=None))).picture
    except:
      return False
   
  def get(self):
    media = self.get_data()

    if media:
      self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
      self.response.headers["Content-Type"] = str(media.mimetype) + "; charset=\"utf-8\""
      self.response.headers["Content-Length"] = media.size
      if self.request.get('force', default_value=False):
          self.response.headers["Content-Disposition"] = "attachment; filename=\""+str(media.filename)+"\""
      else: self.response.headers["Content-Disposition"] = "inline; filename=\""+str(media.filename)+"\""

      self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
      
      self.response.headers["Content-Transfer-Encoding"] = "binary"
      self.response.out.write(media.stream)
      return
     
    self.redirect("/plugins/erepublik/static/squad.png", True)

RoutingTable = [('/plugins/erepublik/soldiers', Soldiers),
                        ('/plugins/erepublik/soldiers-ladder', SoldiersLadder),
                        ('/plugins/erepublik/soldier-.*', SoldierDetails),
                        ('/plugins/erepublik/companies', Companies),
                        ('/plugins/erepublik/company-.*', CompanyDetails),
                        ('/plugins/erepublik/squad-.*', SquadDetails),
                        ('/plugins/erepublik/get-squad-picture', GetSquadPicture)]

application = webapp.WSGIApplication(RoutingTable, gpress.Debug)



def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()