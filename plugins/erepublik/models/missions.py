#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = []

skip_fields = ['key']

from datetime import datetime

from application.gpress import tools
from application.gpress import models
from google.appengine.ext import db

class Mission(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	codename = db.StringProperty()

	""" Utils """
	def get_kind(self):
		return 'Mission'
			
	def update(self, values_dict={}):
		self.categories_keys = []
		self.tags = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "publish_status":
				self.publish_status = int(value)
			elif name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "publish_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.publish_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "image":
				if value:
					self.image = db.Key(value)
				else:
					self.image = None
			elif name == "permalink":
				self.permalink = tools.key_name(str(value).strip())
			elif name == "categories_keys" and value:
				self.categories_keys.extend((",".join([str(x) for x in value])).split(","))
				#self.categories_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.categories_keys.count(""):
					#self.categories_keys.remove("")
			elif name == "tags":
				self.tags.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.tags.count(""):
					self.tags.remove("")
						
			elif name not in skip_fields and name in self.properties():
				if isinstance(self.__getattribute__(name), db.IntegerProperty):
					self.__setattr__(name, int(str(value).strip()))
				elif isinstance(self.__getattribute__(name), db.FloatProperty):
					self.__setattr__(name, float(str(value).strip()))
				else:
					self.__setattr__(name, str(value).strip())