#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developers Network
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.gpress import models
from application.models import media
from google.appengine.ext import db
import datetime
Media = media.Media

skip_fields = ['key' ]

no_html_fields = [
                  'username'
                  ]

company_integers = [
                  'id',
                  'quality',
                  'stock'
                  ]

company_floaters = [
                  'raw_materials_in_stock'
                  ]

company_strings = [
                  'domain',
                  'industry',
                  'name',
                  'country',
                  'region'
                  ] 

company_booleans = [
                  'is_for_sale'
                  ]

company_references = [
                  'picture'
                  ]

company_lists = [
                 'job_offers',
                 'export_licences',
                 'employees'
                 ]

company_datetimes = [
                  'date',
                  'update_date'
                  ]

class Company(models.SerializableModel):
  """ Timelines """
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True)
  update_date = db.DateTimeProperty(auto_now=True)
  update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
  
  picture = db.ReferenceProperty()
  name = db.StringProperty()
  notes = db.TextProperty()

  is_for_sale = db.BooleanProperty()
  id = db.IntegerProperty(required=True)
  country = db.StringProperty()
  region = db.StringProperty()
  domain = db.StringProperty()
  industry = db.StringProperty()
  quality = db.IntegerProperty()
  stock = db.IntegerProperty()
  raw_materials_in_stock = db.FloatProperty()
  
  job_offers = db.StringListProperty()
  export_licences = db.StringListProperty()
  employees = db.StringListProperty()
  
  """ Utils """
  def get_kind(self):
    return 'Company'
        
  def soldiers_count(self):
    from plugins.erepublik.models import troops
    return troops.Soldier.all().filter('assigned_company =', str(self.key())).count(1000)
        
  def soldiers(self):
    from plugins.erepublik.models import troops
    return troops.Soldier.all().filter('assigned_company =', str(self.key())).fetch(1000)
        
  def employers_url(self):
    return '//%s' % self.employer
  
  def profile_url(self):
    return '//%s/redirect' % self.employer
  
  def industry_image(self):
    if self.industry == 'weapon':
      return 'http://img69.imageshack.us/img69/1003/gun128x128.png'
    if self.industry == 'moving tickets':
      return 'http://www.screamfestla.com/images/old-movie-ticket.png'
    if self.industry == 'iron':
      return 'http://images3.wikia.nocookie.net/paragon/images/1/17/Salvage_Iron.png'
    if self.industry == 'food':
      return 'http://icons.iconseeker.com/png/128/japanese-stuff/food-1.png'
    if self.industry == 'gift':
      return 'http://www.funagain.com/images/main/gift-icon-small.png'
    if self.industry == 'diamonds':
      return 'http://upload.wikimedia.org/wikipedia/commons/e/e2/Rocks\'n\'Diamonds.png'
    if self.industry == 'grain':
      return 'http://wiki.openttd.org/images/6/66/Grain.png'
    if self.industry == 'hospital':
      return 'http://icons.iconseeker.com/png/fullsize/perfect-city/hospital.png'
    if self.industry == 'wood':
      return 'http://www.goldenweb.it/software/immagini/icone/miscellanea/box_container/Wood%20Container.gif'
    if self.industry == 'defensesystem':
      return 'http://www.androidup.com/wp-content/uploads/2009/05/retro-defence-icone.png'
    if self.industry == 'house':
      return 'http://icons.iconseeker.com/png/fullsize/the-real-christmas-05/house-1.png'
    if self.industry == 'oil':
      return 'http://wikiwealth.wdfiles.com/local--files/pics3/crude_oil.png'

    return 'http://images3.wikia.nocookie.net/paragon/images/1/17/Salvage_Iron.png'
  
  def domain_image(self):
    return 'http://www.erepublik.com/images/parts/icon_domain_%s.gif' % self.domain
  
  def country_image(self):
    return "http://www.erepublik.com/images/flags/M/%s.gif" % self.country
  
  def update(self, values_dict={}):
    self.picture = None
    for name in [x.replace('-','_').strip() for x in values_dict]:
      value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
      if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
        value = value[0]
      if name not in skip_fields and name in self.properties().keys():
        if name in company_integers:
          self.__setattr__(name, int(str(value).strip()))
        elif name in company_floaters:
          self.__setattr__(name, float(str(value).strip()))
        elif name in company_strings:
          self.__setattr__(name, str(value).strip())
        elif name in company_booleans:
          self.__setattr__(name, bool(int(value.strip())))
        elif name in company_references:
          self.__setattr__(name, db.Key(str(value).strip()))
        elif name in company_datetimes:
          date = value.split(' ')
          year, month, day = date[0].split('-')
          hours, minutes, seconds = date[1].split(':')
          self.__setattr__(name, datetime.datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0])))
        elif isinstance(self.__getattribute__(name), db.IntegerProperty):
          self.__setattr__(name, int(str(value).strip()))
        elif isinstance(self.__getattribute__(name), db.FloatProperty):
          self.__setattr__(name, float(str(value).strip()))
        else:
          self.__setattr__(name, str(value).strip())