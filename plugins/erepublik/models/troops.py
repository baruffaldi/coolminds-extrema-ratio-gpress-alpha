#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developers Network
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""
from __future__ import division

from application.gpress import models, tools
from application.models import media
from google.appengine.ext import db
import datetime
Media = media.Media

skip_fields = ['key' ]

no_html_fields = [
                  'username'
                  ]

soldier_integers = [
                  'id',
                  'level',
                  'experience_points',
                  'fights',
                  'citizenship_c_id',
                  'country_id',
                  'region_id',
                  'employer_id',
                  'newspaper_id'
                  ]

soldier_floaters = [
                  'wellness',
                  'damage',
                  'strength'
                  ]

soldier_strings = [
                  'realname',
                  'reallocation',
                  'realage',
                  'realjob',
                  'name',
                  'newspaper',
                  'employer',
                  'country',
                  'citizenship_country',
                  'assigned_company',
                  'assigned_squad',
                  'military_rank',
                  'sex',
                  'avatar_link',
                  'forum'
                  ]

soldier_booleans = [
                  'is_general_manager',
                  'is_party_member',
                  'is_president',
                  'is_congressman',
                  'assigned_squad_leadership',
                  'assigned_company_leadership'
                  ]

soldier_references = [
                  'picture'
                  ]

soldier_lists = [
                  'skills',
                  'medals'
                  ]

soldier_datetimes = [
                  'date_of_birth',
                  'realbirthdate',
                  'date',
                  'update_date'
                  ]

class Soldier(models.SerializableModel):
  """ Timelines """
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True)
  update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
  update_date = db.DateTimeProperty(auto_now=True)
  
  realname = db.StringProperty()
  reallocation = db.StringProperty()
  realbirthdate = db.DateTimeProperty()
  realjob = db.StringProperty()
  forum = db.StringProperty()
  picture = db.ReferenceProperty()
  
  assigned_company = db.StringProperty()
  assigned_company_leadership = db.BooleanProperty()
  def assigned_company_name(self):
    if self.assigned_company:
      return db.get(db.Key(self.assigned_company)).name
    return ""
  def get_assigned_company(self):
    if self.assigned_company:
      return db.get(db.Key(self.assigned_company))
    return {}
  
  assigned_squad = db.StringProperty()
  assigned_squad_leadership = db.BooleanProperty()
  def assigned_squad_name(self):
    if self.assigned_squad:
      return Squad.get(db.Key(self.assigned_squad)).name
    return ""
  def get_assigned_squad(self):
    if self.assigned_squad:
      return Squad.get(db.Key(self.assigned_squad))
    return {}
  
  notes = db.TextProperty()

  """ Source Info """
  name = db.StringProperty(required=True)
  id = db.IntegerProperty()
  avatar_link = db.TextProperty()
  sex = db.StringProperty(default="M")
  level = db.IntegerProperty()
  experience_points = db.IntegerProperty()
  wellness = db.FloatProperty()
  strength = db.FloatProperty()
  fights = db.IntegerProperty()
  military_rank = db.StringProperty()
  def military_rank_int(self):
    if self.military_rank == 'Private': return 0
    if self.military_rank == 'Corporate': return 1
    if self.military_rank == 'Sergeant': return 2
    if self.military_rank == 'Lieutenant': return 3
    if self.military_rank == 'Captain': return 4
    if self.military_rank == 'Colonel': return 5
    if self.military_rank == 'General': return 6
    if self.military_rank == 'Field Marshal': return 7
    return -1
  
  damage = db.FloatProperty()
  damage_when_joined = db.FloatProperty()
  #damage_since_joined = db.FloatProperty(default=0.0)
  def damage_since_joined(self):
    return int(self.damage - self.damage_when_joined)
  
  citizenship_country = db.StringProperty()
  citizenship_c_id = db.IntegerProperty()
  country = db.StringProperty()
  country_id = db.IntegerProperty()
  region = db.StringProperty()
  region_id = db.IntegerProperty()
  employer = db.StringProperty()
  employer_id = db.IntegerProperty()
  newspaper = db.StringProperty()
  newspaper_id = db.IntegerProperty()
  is_general_manager = db.BooleanProperty()
  is_party_member = db.BooleanProperty()
  is_president = db.BooleanProperty()
  is_congressman = db.BooleanProperty()
  medals = db.StringListProperty()
  # [( type, amount ),( type, amount )]
  skills = db.StringListProperty()
  # [( domain, value ),( domain, value )]
  date_of_birth = db.DateTimeProperty()
  
  """ Utils """
  def get_kind(self):
    return 'Soldier'
  
  def get_hands_damage(self):
    return int(0.5 * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)
  
  def get_q1_damage(self):
    return int((1+(1/5)) * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)
  
  def get_q2_damage(self):
    return int((1+(2/5)) * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)
  
  def get_q3_damage(self):
    return int((1+(3/5)) * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)
  
  def get_q4_damage(self):
    return int((1+(4/5)) * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)
  
  def get_q5_damage(self):
    return int((1+(5/5)) * (1+(self.military_rank_int()/5)) * self.strength * (1 + (self.wellness - 25) / 100) * 2)

  def is_out_of_job(self):
    company = db.get(self.assigned_company)
    return company.name != self.employer
        
  def company_url(self):
    return 'http://www.erepublik.com/en/company/%i' % self.employer_id
        
  def newspaper_url(self):
    return 'http://www.erepublik.com/en/newspaper/%i' % self.newspaper_id
  
  def profile_url(self):
    return 'http://www.erepublik.com/en/citizen/profile/%i' % self.id
  
  def sex_image(self):
    return "/plugins/erepublik/static/%s.png" % self.sex.lower()
  
  def military_rank_image(self):
    return "http://www.erepublik.com/images/parts/icon_position_military_%s.gif" % tools.key_name(self.military_rank).replace('-','')

  def citizenship_country_image(self):
    return " http://www.erepublik.com/images/flags/M/%s.gif" % self.citizenship_country
  
  def country_image(self):
    return " http://www.erepublik.com/images/flags/M/%s.gif" % self.country
  
  def avatar_big_link(self):
    url = str(self.avatar_link).split("/")
    uuid = url.pop().split("_")[0]
    return '%s/%s%s' % ("/".join(url), uuid, ".jpg")

  def update(self, values_dict={}):
    self.assigned_squid = None
    self.assigned_company = None
    for name in [x.replace('-','_').strip() for x in values_dict]:
      if name not in skip_fields:
        value = str(values_dict[name]).strip()
        if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
          value = value[0]
        if name in self.properties().keys():
          if name in soldier_integers:
            self.__setattr__(name, int(str(value).strip()))
          elif name in soldier_floaters:
            self.__setattr__(name, float(str(value).strip()))
          elif name in soldier_strings:
            self.__setattr__(name, str(value).strip())
          elif name in soldier_booleans:
            self.__setattr__(name, bool(int(value.strip())))
          elif name in soldier_references and value:
            self.__setattr__(name, db.Key(str(value).strip()))
          elif name in soldier_datetimes and value:
            date = value.split(' ')
            year, month, day = date[0].split('-')
            hours, minutes, seconds = date[1].split(':')
            self.__setattr__(name, datetime.datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0])))
          elif isinstance(self.__getattribute__(name), db.IntegerProperty):
            self.__setattr__(name, int(str(value).strip()))
          elif isinstance(self.__getattribute__(name), db.FloatProperty):
            self.__setattr__(name, float(str(value).strip()))
          elif value:
            self.__setattr__(name, str(value).strip())

class Squad(models.SerializableModel):
  """ Timelines """
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True)
  update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
  update_date = db.DateTimeProperty(auto_now=True)
  
  picture = db.ReferenceProperty(Media)
  name = db.StringProperty(required=True)
  notes = db.TextProperty()
  date_of_birth = db.DateTimeProperty(auto_now_add=True)
        
  def profile_url(self):
    return '/plugins/erepublik/squad-%s' % str(self.key())

  """ Source Info """
  def soldiers_count(self):
    return Soldier.all().filter('assigned_squad =', str(self.key())).count(1000)
  
  def soldiers(self):
    return Soldier.all().filter('assigned_squad =', str(self.key())).fetch(1000)