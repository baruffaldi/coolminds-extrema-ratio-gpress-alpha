#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os


from google.appengine.ext.webapp import template
from google.appengine.api import memcache, users
from google.appengine.ext import webapp
from django.utils.datastructures import SortedDict

from application.utils.jscompress.jsmin import jsmin
from django.template import Node
#----------------------------------

register = webapp.template.create_template_register()
  
class StringNode(Node):
    def __init__(self, s):
        self.s = s
    def render(self, context):
        return self.s

class ContactFormNode(Node):
  def __init__(self, nametext="Name",referencetext="Email or Phone",sendtext="Send"):
    #todo: globalization
    self.name = users.get_current_user().nickname() if users.get_current_user() else ""
    self.reference = users.get_current_user().email() if users.get_current_user() else ""
    self.referencetext = referencetext
    self.nametext = nametext
    self.sendtext = sendtext
  def render(self, context):
    if context.get("gpress",{}) and context.get("gpress",{}).user.is_user():
      users = context.get("gpress",{}).public_contents.users(guest=0,users_messages=1)
      select = ""
      for u in users:
        select += "<option value='%s'>%s</option>" % ( u.key(), u.name )
      self.s = """<form action="/mailbox/send" method="post" class="gpress-mailbox-send-form">
<p>%s <i>%s</i></p>
<p>Your Email <i>%s</i></p>
<p><label>%s</label> <input type="text" name="name" size="32" /></p>
<p><label>%s</label> <input type="text" name="reference" size="32" /></p>
<p><label>Destinations</label> <select name="destinations">
%s
</select></p>
<p><textarea name="body" cols="35" rows="3"> </textarea></p>
<p><input type="submit" name="send" value="%s" /></p>
</form>""" % (self.nametext, self.name, self.reference, self.nametext, self.referencetext, select, self.sendtext)
    elif users.get_current_user():
      self.s = """<form action="/mailbox/send" method="post" class="gpress-mailbox-send-form">
<p>%s <i>%s</i></p>
<p>Email <i>%s</i></p>
<p><label>%s</label> <input type="text" name="name" size="32" /></p>
<p><label>%s</label> <input type="text" name="reference" size="32" /></p>
<p><textarea name="body" cols="35" rows="3"> </textarea></p>
<p><input type="submit" name="send" value="%s" /></p>
</form>""" % (self.nametext, self.name, self.reference, self.nametext, self.referencetext, self.sendtext)
    else: 
      self.s = """<form action="/mailbox/send" method="post" class="gpress-mailbox-send-form">
<p><label>%s</label> <input type="text" name="name" size="32" /></p>
<p><label>%s</label> <input type="text" name="reference" size="32" /></p>
<p><textarea name="body" cols="35" rows="3"> </textarea></p>
<p><input type="submit" name="send" value="%s" /></p>
</form>""" % (self.nametext, self.referencetext, self.endtext)
    return self.s

@register.tag
def contacts_form(parser, token):
  try:
    params = token.split_contents()
    if params.__len__() > 2:
      return ContactFormNode( nametext=params[2] if params.__len__() > 2 else "" )
    elif params.__len__() > 3:
      return ContactFormNode(
        nametext=params[2] if params.__len__() > 2 else "",
        referencetext=params[3] if params.__len__() > 3 else ""
      )
    elif params.__len__() > 4:
      return ContactFormNode(
        nametext=params[2] if params.__len__() > 2 else "",
        referencetext=params[3] if params.__len__() > 3 else "",
        send=params[4] if params.__len__() > 4 else ""
      )
    else:
      return ContactFormNode()
  except ValueError:
    return StringNode("")
contacts_form.is_safe = True
@register.tag
def fopen(parser, token):
  try:
    tag_name, f = token.split_contents()
  except ValueError:
    raise template.TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
  if not (f[0] == f[-1] and f[0] in ('"', "'")):
    raise template.TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
  try:
    memcache.flush_all()
    from application.utils.md5sum import md5sum
    basepath = os.path.dirname(__file__)
    filepath = ("../../.." if f[1:-1].split("/")[1] not in ["plugins", "themes"] else "../.." )+f[1:-1];
    filemd5 = "file_"+str(md5sum(basepath+filepath))
    ret = memcache.get(filemd5)
    if not ret:
      fs = open(filepath, "r")
      ret = fs.read()
      fs.close()
      memcache.set(filemd5, ret,864000)
    return StringNode(ret)
  except ValueError:
      return StringNode("")
fopen.is_safe = True

@register.filter
def csscompress(script):
  return jsmin(script)
csscompress.is_safe = True

@register.filter
def jscompress(script):
  return jsmin(script)
jscompress.is_safe = True

@register.filter
def to_str(s):
  return str(s).strip()
to_str.is_safe = True

@register.filter
def isequal(v,w):
  return str(v).strip() == str(w).strip()
isequal.is_safe = True

_KEY_NAME_PATTERN = r"\w+"
@register.filter
def key_name(string):
  import re
  r = re.compile(_KEY_NAME_PATTERN)
  return "-".join([x for x in r.findall(string)]).lower()
key_name.is_safe = True

@register.filter
def strreplace(v,w,wv):
  return str(v).replace(w,wv)
strreplace.is_safe = True
  
@register.tag
def jsfilecompress(parser, token):
  try:
      tag_name, value = token.split_contents()
  except ValueError:
      raise template.TemplateSyntaxError, "%r tag requires a single argument" % token.contents.split()[0]
  if not (value[0] == value[-1] and value[0] in ('"', "'")):
      raise template.TemplateSyntaxError, "%r tag's argument should be in quotes" % tag_name
  try:
    from application.utils.md5sum import md5sum
    filepath = ("../../.." if value[1:-1].split("/")[1] in ["plugins", "themes"] else "../../static" )+value[1:-1];
    filemd5 = "js_"+str(md5sum(os.path.abspath(filepath)))

    js = memcache.get(filemd5)
    if not js:
      file = open(os.path.abspath(filepath))
      js = jsmin(file.read())
      memcache.set(filemd5, js,864000)
      file.close()
    return StringNode(js)
  except: return StringNode("null;")
jsfilecompress.is_safe = True

@register.filter
def get_unique_lang(grouped_list, lang):
  ret = []
  permalinks = []
  for language in grouped_list:
    if language["grouper"] == lang:
      ret.extend(language["list"])
      for x in ret:
        permalinks.append(x.permalink)
        
  for language in grouped_list:
    if language["grouper"] != lang:
      missing_entries = []
      for entry in language["list"]:
        if not entry.permalink in permalinks:
          permalinks.append(entry.permalink)
          missing_entries.append(entry)
      missing_entries.reverse()
      ret.extend(missing_entries)
  return ret
get_unique_lang.is_safe = True

@register.filter(name='sort')
def listsort(value):
  if isinstance(value,dict):
    new_dict = SortedDict()
    key_list = value.keys()
    key_list.sort()
    for key in key_list:
      new_dict[key] = value[key]
    return new_dict
  elif isinstance(value, list):
    new_list = list(value)
    new_list.sort()
    return new_list
  else:
    return value
listsort.is_safe = True

@register.filter
def have_value(value,what):
  if isinstance(value,dict):
    return True if str(what).strip().lower() in value.values() else False
  elif isinstance(value, list):
    return True if str(what).strip().lower() in value else False
  else:
    return False
have_value.is_safe = True

@register.filter
def dict_to_list(value):
  if isinstance(value,dict):
    list = []
    for key in value.keys():
      list.append(value.get(key))
    return list
  else:
    return []
dict_to_list.is_safe = True

@register.filter
def have_string_value(value,what):
  if isinstance(value,dict):
    return True if str(what).lower() in str(" ".join([str(x).lower() for x in value.values()])).split(' ') else False
  elif isinstance(value, list):
    return True if str(what).lower() in str(" ".join([str(x).lower() for x in value])).split(' ') else False
  else:
    return True if what and value and str(what).lower() in list(value).lower() else False
have_string_value.is_safe = True

@register.filter
def have_key(value,what):
  return True if what in value.keys() else False
have_key.is_safe = True

@register.filter
def join2string(list,var,dictparam=None):
  ret = ""
  for v in list:
    ret += "%s'%s'" % (var, v.__getattr__(dictparam) if dictparam else v)
  
  return ret[len(var):]
join2string.is_safe = True

@register.filter
def joinlabelsname(list,var):
  ret = ""
  for v in list:
    ret += "%s'%s'" % (var, v['name'])
  
  return ret[len(var):]
joinlabelsname.is_safe = True

@register.filter
def get_value(obj, value):
  return dict(obj).get(value)
get_value.is_safe = True

@register.filter
def days_from_date(value,what=False):
  from datetime import date, datetime
  try:
    if what:
      year, month, day = str(value).split(' ')[0].split('-')
      value = date(int(year),int(month),int(day))
      year, month, day = what.split(' ')[0].split('-')
      return (value-date(int(year),int(month),int(day))).days
    else:
      year, month, day = str(value).split(' ')[0]
      if value:
        return (date.today()-date(int(year),int(month),int(day))).days
  except:
    return "n/a"
days_from_date.is_safe = True

@register.filter
def to_dict(obj):
  return dict(obj)
to_dict.is_safe = True

@register.filter
def to_list(obj):
  return list(obj)
to_list.is_safe = True

@register.filter
def split_to_list(obj,sep=""):
  return str(obj).split(sep)
split_to_list.is_safe = True

@register.filter
def get(obj,key):
  return obj.__getitem__(key)
get.is_safe = True


@register.filter
def remove_var(value,var):
  ret = []
  
  for v in value.split('&'):
    if var.count('='):
      if var != v:
        ret.append(v) 
    elif var != v[:len(var)]:
      ret.append(v)
      
  return "&".join([x for x in ret])
remove_var.is_safe = True

@register.filter
def get_url_hostname(value="/////"):
  return str(str(value).split('/')[2]).split(':')[0]
get_url_hostname.is_safe = True

@register.filter
def truncate_title(value,length=30):
  if len(value) > length:
    return "%s&#8230;" % str(value)[:length]
  return str(value)
truncate_title.is_safe = True

@register.tag
def urlvars(parser, token):
  try:
    tag_name, varname, value = token.split_contents()
    request_vars = os.environ["QUERY_STRING"].split('&')
    test_1 = "%s=%s" % (varname,value)
    test_2 = "%s=-%s" % (varname,value)
    if test_1 in request_vars:
      request_vars.remove(test_1)
    if test_2 in request_vars:
      request_vars.remove(test_2)
    
    return ReturnString("&".join([x for x in request_vars]))
  except:
    try:
      tag_name, varname = token.split_contents()
      request = os.environ["QUERY_STRING"]
      request_vars = request.split('&')
      ret = []
      for var in value.split('&'):
        if varname != var[:len(varname)] and var not in ret:
          ret.append(var)
          
      return "&".join([x for x in ret])
    except:
      return ReturnString(os.environ["QUERY_STRING"])
    
class ReturnString():
    def __init__(self, string):
        self.string = string
    def render(self, context):
        return self.string