#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os, re, datetime

from google.appengine.ext.webapp import template
from google.appengine.api import images
from application.utils.cookies import Cookies

from django.utils import translation

template.register_template_library('application.gpress.template_helpers')

def get_requested_object_list(start=0, depth=0):
    req = str(os.environ["PATH_INFO"]).split('/')[start:]
    if not len(req):
        return False
    
    ret = []
    c = 0
    for v in req:
      if not depth or c < depth:
        if v != '' and v != None:
          ret.append(v)
      c += 1
            
    return ret

def get_requested_object_string(start=0, depth=0, separator=""):
    return separator.join([x for x in get_requested_object_list(start,depth)])

def get_templates(gp, tpl):
    templates = []
    if gp.page.admin:
        templates += '%stemplate.html' % gp.site.admin_templates_path
        templates += '%s%s/%s.html' % (gp.page.controller, tpl)
    else:
        templates += '%stemplate.html' % gp.site.templates_path
        templates += '%s.html' % tpl
        
    return templates

def render(handler, gp=None, tpl='main', tplvars={}):
    if gp is None:
        from application import gpress
        gp = gpress.Registry(handler)
        
    p = tplvars["article"] if tplvars.has_key('article') else tplvars["page"] if tplvars.has_key('page') else tplvars["post"] if tplvars.has_key('post') else tplvars["media"] if tplvars.has_key('media') else tplvars["file"] if tplvars.has_key('file') else tplvars["link"] if tplvars.has_key('link') else False
    if p: gp.page.process_page(p)
    
    check = os.environ["PATH_INFO"].split("/")
    gp.newLanguage = True if check[-2] == "languages" and check[-1] == "add" else False
    if gp.admin:
        main_tpl = '%stemplate.html' % gp.site.admin_templates_path
        if gp.module:
            sub_tpl = '%sviews/admin/%s.html' % (gp.plugins[0].path, tpl)
        else:
            sub_tpl = '%s%s/%s.html' % (gp.site.admin_templates_path, gp.controller, tpl)
    else:
        main_tpl = '%s/template.html' % gp.site.templates_path
        if gp.module:
            sub_tpl = '%sviews/public/%s.html' % (gp.plugins[0].path, tpl)
        else:
            sub_tpl = '%s%s.html' % (gp.site.templates_path, tpl)

    if gp.site.is_ajax_request:
      gp.page.body = tplvars
    else:
      vars = {'gpress':gp}
      vars.update(tplvars)
      gp.page.body = template.render(sub_tpl, vars, gp.site.debug)


    #-- Se non è autorizzato, redirect 403
    #-- se non trova il template, redirect al template per il 404 ( permette la forzatura della disabilitazione di determinati moduli direttamente dal template )
    parseHandler(handler, gp)
    if tpl == 'intro':
      handler.response.out.write( gp.page.body )
    else:
      handler.response.out.write( template.render(main_tpl, {'gpress':gp}, gp.site.debug) )

def parseHandler(handler, gp=None):
  if gp:
    gp.cookies = Cookies(handler)

    if handler.request.get('l', default_value=False):
      gp.cookies["django_language"] = handler.request.get('l')
    else:
      if not gp.cookies.has_key("django_language"):
        gp.cookies['django_language'] = gp.site.language

    gp.user.language = gp.cookies["django_language"]

    handler.request.COOKIES = gp.cookies
    handler.request.META = os.environ
    language = translation.get_language_from_request(handler.request)
    translation.activate(language)
    handler.request.LANGUAGE_CODE = translation.get_language()
    handler.response.headers['Content-Language'] = translation.get_language()
  handler.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'

def render_error(handler, gp=None, error={}):
    from application import gpress
    if gp is None:
        gp = gpress.Registry()

    gp.page.controller = "feedback"
    gp.page.title = "Error"
    gp.page.id = "error"
    gp.page.description = "Error %s" % error.code if error.has_key('code') else None

    render(handler, gp, 'error', error)


def get_url_var(name):
    params = get_requested_object_list()
    if name in params:
        o = params.index(name)
        if params.__len__() >> o:
            return params[o + 1]
    return False

def get_offset():
    return get_url_var('offset')

def get_paging(entries, kind):
    offset = get_offset()
    paging = {}
    paging["offset"] = offset + 1
    paging["length"] = offset + len(entries)
    paging["total"] = kind.all().count(1000)
    return paging

_KEY_NAME_PATTERN = r"\w+"
def key_name(string, token=False):
  r = re.compile(_KEY_NAME_PATTERN)
  if token:
    return '%s-%s' % ("-".join([x for x in r.findall(string)]).lower(), "".join([x for x in r.findall(str(datetime.datetime.now()))]))
  else:
    return "-".join([x for x in r.findall(string)]).lower()
  

def process_image_request(image, handler=None):
  if handler:
      if handler.request.get('width', default_value=False) or handler.request.get('height', default_value=False):
        width = handler.request.get('width', default_value="0")
        height = handler.request.get('height', default_value="0")
        if width.strip() == '':
          width = 0
        if height.strip() == '':
          height = 0
        image.resize(int(width), int(height))
      
      if handler.request.get('imfeelinglucky', default_value=False):
        image.im_feeling_lucky()
      
      if handler.request.get('flip', default_value=False, allow_multiple=True):
        flips = handler.request.get('flip', allow_multiple=True)
        if 'x' in flips:
          image.vertical_flip()
        if 'y' in flips:
          image.horizontal_flip()
      
      if handler.request.get('rotate', default_value=False):
        image.rotate(int(handler.request.get('rotate')))
      
      if handler.request.get('left_x', default_value=False) or handler.request.get('top_y', default_value=False) or handler.request.get('right_x', default_value=False) or handler.request.get('bottom_y', default_value=False):
        lx = handler.request.get('left_x', default_value=0).strip()
        rx = handler.request.get('right_x', default_value=0).strip()
        ty = handler.request.get('top_y', default_value=0).strip()
        by = handler.request.get('bottom_y', default_value=0).strip()
        if by.strip() == '':
          by = 0
        if ty.strip() == '':
          ty = 0
        if rx.strip() == '':
          rx = 0
        if lx.strip() == '':
          lx = 0
        image.crop(float(lx), float(ty), float(rx), float(by))
            
      if handler.request.get('format') == 'jpeg':
        encoding = images.JPEG
        mimetype = 'image/jpeg'
      elif handler.request.get('format') == 'png':
        encoding = images.PNG
        mimetype = 'image/png'
      else:
        encoding = images.PNG
        mimetype = 'image/png'
      
      try:
        image.execute_transforms(output_encoding=encoding)
      except:
        ""
      
      handler.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
      handler.response.headers["Content-Length"] = len(image._image_data)
      handler.response.headers["Content-Type"] = "%s; charset=\"utf-8\"" % mimetype
      handler.response.headers["Content-Disposition"] = "inline; filename=\"gpress-random-image.%s\"" % mimetype.split('/')[1]
      handler.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
      handler.response.headers["Content-Transfer-Encoding"] = "binary"
      handler.response.out.write(image._image_data)