#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developers Network
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""
from django.template import Node

from google.appengine.api import users
from google.appengine.ext import webapp

register = webapp.template.create_template_register()

class StringNode(Node):
    def __init__(self, s):
        self.s = s
    def render(self, context):
        return self.s

class ContactFormNode(Node):
  def __init__(self, nametext="Name",referencetext="Email or Phone",sendtext="Send"):
    #todo: globalization
    name = users.get_current_user().nickname() if users.get_current_user() else ""
    reference = users.get_current_user().email() if users.get_current_user() else ""
    if users.get_current_user():
      self.s = """<form action="/gp-admin/mailbox/send" method="post" class="gpress-mailbox-send-form">
<p>%s <i>%s</i><input type="hidden" name="name" value="%s" /></p>
<p>%s <i>%s</i><input type="hidden" name="reference" value="%s" /></p>
<p><textarea name="body" cols="35" rows="3"> </textarea></p>
<p><input type="submit" name="send" value="%s" /></p>
</form>""" % (nametext, name, name, referencetext, reference, reference, sendtext)
    else: 
      self.s = """<form action="/gp-admin/mailbox/send" method="post" class="gpress-mailbox-send-form">
<p><label>%s</label> <input type="text" name="name" size="32" /></p>
<p><label>%s</label> <input type="text" name="reference" size="32" /></p>
<p><textarea name="body" cols="35" rows="3"> </textarea></p>
<p><input type="submit" name="send" value="%s" /></p>
</form>""" % (nametext, referencetext, sendtext)
  def render(self, context):
    return self.s

@register.tag
def contacts_form(parser, token):
  try:
    params = token.split_contents()
    return ContactFormNode(
        nametext = params[2] if params.__len__() > 2 else None,
        referencetext = params[3] if params.__len__() > 3 else None,
        sendtext = params[4] if params.__len__() > 4 else None
    )
  except ValueError:
    return StringNode("")
contacts_form.is_safe = True