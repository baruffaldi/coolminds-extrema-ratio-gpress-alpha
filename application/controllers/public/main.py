#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""
import os, datetime

from application.models import pages, posts, news
from application.gpress import tools
from application import gpress 

from google.appengine.api import users
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

Page = pages.Page
PageComment = pages.PageComment
Post = posts.Post
PostComment = posts.PostComment
News = news.News


class Home(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    
    gp.page.title = "Homepage"
    gp.page.url = "homepage"
    gp.controller = "posts"
    
    if int(gp.site.homepage) == 3:
      gp.controller = "news"
      articles = News.all().filter('publish_status >=', 1).order('-publish_status').order('-publish_date').fetch(int(gp.site.posts_limit), tools.get_offset())
      tools.render(self, gp, 'news', {'news': articles})
    
    elif int(gp.site.homepage) == 2:
      gp.controller = "pages"
      page = Page.get(db.Key(gp.site.homepage_page))
      if page:
        gp.page.title += " - %s" % page.title
      tools.render(self, gp, 'page', {'page': page})
    
    elif int(gp.site.homepage) == 1:
      gp.controller = "posts"
      post = Post.get(db.Key(gp.site.homepage_post))
      if post:
        gp.page.title += " - %s" % post.title
      tools.render(self, gp, 'post', {'post': post})

    elif int(gp.site.homepage) == -1:
      tools.render(self, gp, 'intro')

    else:
      postz = Post.all().filter('publish_status >=', 1).order('-publish_status').order('-publish_date').fetch(int(gp.site.posts_limit), tools.get_offset())
      tools.render(self, gp, 'posts', {'posts': postz})
      
class Robots(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    modules = ""
    if not int(gp.site.news_enabled):
      modules += "\nDisallow: /news/"
    if not int(gp.site.posts_enabled):
      modules += "\nDisallow: /blog/"
    if not int(gp.site.pages_enabled):
      modules += "\nDisallow: /pages/"
    if not int(gp.site.files_enabled):
      modules += "\nDisallow: /files/"
    if not int(gp.site.media_enabled):
      modules += "\nDisallow: /media/"
    if not int(gp.site.links_enabled):
      modules += "\nDisallow: /links/"
    if not int(gp.site.contacts_enabled):
      modules += "\nDisallow: /contacts/"
      
    self.response.headers["Content-Type"] = 'text/plain'
    self.response.out.write("""
Sitemap: %s/sitemap.xml.gz
User-agent: *
Disallow: /errors/
Disallow: /gp-admin/
%s    """ % ( gpress.Site().url, modules ) )


class GVerify(webapp.RequestHandler):
  def get(self):
    self.response.out.write('google-site-verification: %s' % str(os.environ['PATH_INFO']).split('/')[-1])

      
class Logout(webapp.RequestHandler):
  def get(self):
    self.redirect( users.create_logout_url( self.request.get( 'requrl', default_value="/" ) ), True )


class Login(webapp.RequestHandler):
  def get(self):
    self.redirect( users.create_login_url( self.request.get( 'requrl', default_value="/gp-admin" ) ) )


application = webapp.WSGIApplication([('/', Home),
                                                          ('/logout', Logout),
                                                          ('/login', Login),
                                                          ('/robots.txt', Robots),
                                                          ('/google.*.html', GVerify)],
                                                          gpress.Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()