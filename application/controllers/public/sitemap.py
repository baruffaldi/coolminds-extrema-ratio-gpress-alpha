#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import zlib

from application.models import posts, pages, media, files, links, contacts, news
from application import gpress

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app


class SiteMap(webapp.RequestHandler):
  def get(self):
    data = []
    data.extend(news.News.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(posts.Post.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(pages.Page.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(media.Media.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(files.File.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(links.Link.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(contacts.Contact.all().fetch(1000))
    
    xml = template.render('../../views/feeds/sitemap.xml', {'entries': data, 'gpress': gpress.Registry()})
    
    self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
    self.response.headers["Content-Type"] = 'text/xml; charset=\"utf-8\"'
    self.response.headers["Content-Length"] = len(xml)
    self.response.headers["Content-Disposition"] = "inline; filename=\"sitemap.xml\""
    self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
    self.response.headers["Content-Transfer-Encoding"] = "binary"
    self.response.out.write(xml)

class SiteMapGz(webapp.RequestHandler):
  def get(self):
    data = []
    data.extend(news.News.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(posts.Post.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(pages.Page.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(media.Media.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(files.File.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(links.Link.all().filter('publish_status >=', 1).fetch(1000))
    data.extend(contacts.Contact.all().fetch(1000))
    
    xml = template.render('../../views/feeds/sitemap.xml', {'entries': data, 'gpress': gpress.Registry()})
    gz = zlib.compress(xml, 1)
    
    self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
    self.response.headers["Content-Type"] = 'application/x-compressed; charset=\"utf-8\"'
    self.response.headers["Content-Length"] = len(gz)
    self.response.headers["Content-Disposition"] = "attachment; filename=\"sitemap.xml.gz\""
    self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
    self.response.headers["Content-Transfer-Encoding"] = "binary"
    self.response.out.write(gz)


application = webapp.WSGIApplication([('/sitemap.xml.gz', SiteMapGz),
                                                          ('/sitemap.xml.*', SiteMap)],
                                                           gpress.Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()