#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode
from application import gpress 
from application.gpress import tools
from application.models import posts
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Post = posts.Post
PostComment = posts.PostComment

class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    import datetime
    postz = Post.all().filter('publish_date <=', datetime.datetime.now()).order('-publish_date')
    postz.fetch(int(gp.site.posts_limit), tools.get_offset())
    
    tools.render(self, gp, 'posts', {'posts': postz})


class ReadPost(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    key = tools.get_url_var('blog')
    tools.parseHandler(self, gp)
    
    try: post = Post.get(key)
    except:
      post = Post.all()
      post.filter('permalink =', key)
      if not gp.user.is_contributor():
        post.filter('publish_status >=', 1)
      if post.count(1):
        post = post.fetch(1)[0]
      else:
        post = False
        
    if post:
      if not post.publish_status and not gp.user.is_contributor():
          self.redirect('/errors/forbiddens?%s' %
                 urlencode({'ref': self.request.uri}), True)
          return
          
      gp.page.process_page(post)
      tools.render(self, gp, 'post', {'post': post})
    else:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)


application = webapp.WSGIApplication([('/blog', Main),
                                                          ('/blog/', Main),
                                                          ('/blog/.*', ReadPost)],
                                                          gpress.Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()