#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.models import posts, pages, media, files, links, news, contacts
from application import gpress

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

			
class News(webapp.RequestHandler):
	def get(self):
		data = news.News.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"news.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))
			
class Posts(webapp.RequestHandler):
	def get(self):
		data = posts.Post.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"posts.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))

class Pages(webapp.RequestHandler):
	def get(self):
		data = pages.Page.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"pages.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))
			
class Media(webapp.RequestHandler):
	def get(self):
		data = media.Media.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"media.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))
			
class Files(webapp.RequestHandler):
	def get(self):
		data = files.File.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"files.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))

class Links(webapp.RequestHandler):
	def get(self):
		data = links.Link.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"links.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))

class Contacts(webapp.RequestHandler):
	def get(self):
		data = contacts.Contact.all()
		data.filter('publish_status >=', 1)
		data.filter('feeds_shared =', True)
		data.order('-publish_status')
		data.order('-date')
		data.fetch(1000)
		self.response.headers["Content-Type"] = 'application/rss+xml'
		self.response.headers["Content-Disposition"] = "inline; filename=\"contacts.rss.xml\""
		self.response.out.write(template.render('../../views/feeds/rss.xml', {'entries': data, 'gpress': gpress.Registry()}))

application = webapp.WSGIApplication([('/feeds/news.*', News),
                                                          ('/feeds/posts.*', Posts),
                                                          ('/feeds/blog.*', Posts),
                                                          ('/feeds/pages.*', Pages),
                                                          ('/feeds/media.*', Media),
                                                          ('/feeds/files.*', Files),
                                                          ('/feeds/links.*', Links),
                                                          ('/feeds/contacts.*', Contacts)],
																		 											gpress.Debug)

def main():
	run_wsgi_app(application)
	
if __name__ == "__main__":
	main()