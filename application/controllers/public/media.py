#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import logging

from urllib import urlencode

from application.models import media 
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.api import memcache, images
from google.appengine.ext.webapp.util import run_wsgi_app

Media = media.Media
MediaAlbum = media.MediaAlbum

class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    filez = Media.all().filter('publish_status >=', 1)
    filez.fetch(int(gp.site.media_limit), 0)
        
    tools.render(self, gp, 'media', {'media': filez, 'albums':['Uncategorized'] })
        
        
class Preview(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    
    try: media = Media.get(tools.get_url_var('media'))
    except: 
      file_key = Media.all(keys_only=True)
      file_key.filter('permalink =', tools.get_url_var('media'))
      if not gp.user.is_contributor():
        file_key.filter('publish_status >=', 0)
            
      if file_key.count(1):
        media = Media.get(file_key.fetch(1)[0])
      else:
        self.redirect('/errors/document-not-found?%s' %
               urlencode({'ref': self.request.uri}), True)
        return

    gp.page.process_page(file)
    tools.render(self, gp, 'file', {'file': media})
        
        
class Download(webapp.RequestHandler):
  def get_data(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    key = tools.get_url_var('media')
    media = Media.all()
    media.filter('permalink =', key)
    if not gp.user.is_contributor():
      media.filter('publish_status >=', 0)
    
    if media.count(1):
      m = media.fetch(1)[0]
      if not self.request.get('disable_counter', default_value=False):
        m.counter += 1
        m.save()
        
      return m

    return False
      
  def get(self):
    media = self.get_data()

    if media:
      self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
      self.response.headers["Content-Type"] = str(media.mimetype) + "; charset=\"utf-8\""
      self.response.headers["Content-Length"] = media.size
      if self.request.get('force', default_value=False):
          self.response.headers["Content-Disposition"] = "attachment; filename=\""+str(media.filename)+"\""
      else: self.response.headers["Content-Disposition"] = "inline; filename=\""+str(media.filename)+"\""

      self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
      
      self.response.headers["Content-Transfer-Encoding"] = "binary"
      self.response.out.write(media.stream)
      return

    self.redirect('/errors/document-not-found?' +
           urlencode({'ref': self.request.uri}), True)
        
class CatRandomDownload(webapp.RequestHandler):
  def get_data(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    key = tools.get_url_var('album')
    album = MediaAlbum.all(keys_only=True).filter('title =', key)
    if album.count(1):
      key = str(album.fetch(1)[0])
      media = Media.all().filter('albums_keys IN', [key])
      
      if not gp.user.is_contributor():
        media.filter('publish_status >=', 0)
        
      if media.count(1):
        max = media.count(1000) - 1
        from random import randint
        start = randint(0, max)
        content = media.fetch(1, start)[0]
        if not self.request.get('disable_counter', default_value=False):
          content.counter += 1
          content.save()

        return content
      
  def get(self):
    media = self.get_data()

    if media:
      tools.process_image_request(images.Image(media.stream), self)
      #self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
      #self.response.headers["Content-Type"] = str(media.mimetype) + "; charset=\"utf-8\""
      #self.response.headers["Content-Length"] = media.size
      #if self.request.get('force', default_value=False):
          #self.response.headers["Content-Disposition"] = "attachment; filename=\""+str(media.filename)+"\""
      #else: self.response.headers["Content-Disposition"] = "inline; filename=\""+str(media.filename)+"\""

      #self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
      
      #self.response.headers["Content-Transfer-Encoding"] = "binary"
      #self.response.out.write(media.stream)
      return
    
    self.error(404)
    self.response.headers["X-Gpress-Content-Status"] = False
    #self.redirect('/images/icons/status/gpress-not-found.gif', True)
    return
    #self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
    #self.response.headers["Content-Length"] = len(notfound)
    #self.response.headers["Content-Type"] = "image/gif; charset=\"utf-8\""
    #self.response.headers["Content-Disposition"] = "inline; filename=\"gpress-not-found.gif\""
    #self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
    #self.response.headers["Content-Transfer-Encoding"] = "binary"
    #self.response.out.write(notfound)
        
        
class Thumbnail(webapp.RequestHandler):
  def get_data(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    key = tools.get_url_var('media')
    media = Media.all()
    media.filter('permalink =', key)
    if not gp.user.is_contributor():
      media.filter('publish_status >=', 0)
    
    if media.count(1):
      m = media.fetch(1)[0]
      return m

    return False
  
  def get(self):
    media = self.get_data()
    if media:
      image = images.Image(media.stream)
      
      if self.request.get('width', default_value=False) or self.request.get('height', default_value=False):
        width = self.request.get('width', default_value="0")
        height = self.request.get('height', default_value="0")
        if width.strip() == '':
          width = 0
        if height.strip() == '':
          height = 0
        image.resize(int(width), int(height))
      
      if self.request.get('imfeelinglucky', default_value=False):
        image.im_feeling_lucky()
      
      if self.request.get('flip', default_value=False, allow_multiple=True):
        flips = self.request.get('flip', allow_multiple=True)
        if 'x' in flips:
          image.vertical_flip()
        if 'y' in flips:
          image.horizontal_flip()
      
      if self.request.get('rotate', default_value=False):
        image.rotate(int(self.request.get('rotate')))
      
      if self.request.get('left_x', default_value=False) or self.request.get('top_y', default_value=False) or self.request.get('right_x', default_value=False) or self.request.get('bottom_y', default_value=False):
        lx = self.request.get('left_x', default_value=0).strip()
        rx = self.request.get('right_x', default_value=0).strip()
        ty = self.request.get('top_y', default_value=0).strip()
        by = self.request.get('bottom_y', default_value=0).strip()
        if by.strip() == '':
          by = 0
        if ty.strip() == '':
          ty = 0
        if rx.strip() == '':
          rx = 0
        if lx.strip() == '':
          lx = 0
        image.crop(float(lx), float(ty), float(rx), float(by))
            
      if self.request.get('format') == 'jpeg':
        encoding = images.JPEG
      else:
        encoding = images.PNG
      
      try:
        image.execute_transforms(output_encoding=encoding)
      except:
        ""
      
      self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
      #self.response.headers["Content-Type"] = str(media.mimetype) + "; charset=\"utf-8\""
      self.response.headers["Content-Length"] = len(image._image_data)
      if self.request.get('force', default_value=False):
          self.response.headers["Content-Disposition"] = "attachment; filename=\""+str(media.filename)+"\""
      else: self.response.headers["Content-Disposition"] = "inline; filename=\""+str(media.filename)+"\""
  
      self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
      
      self.response.headers["Content-Transfer-Encoding"] = "binary"
      self.response.out.write(image._image_data)
      return
    
    #self.redirect('/images/icons/status/gpress-not-found.gif', True)
    #return
    #notfound = open('../../static/images/icons/status/gpress-not-found.gif', 'r').read()
    #self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
    #self.response.headers["Content-Length"] = len(notfound)
    #self.response.headers["Content-Type"] = "image/gif; charset=\"utf-8\""
    #self.response.headers["Content-Disposition"] = "inline; filename=\"gpress-not-found.gif\""
    #self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
    #self.response.headers["Content-Transfer-Encoding"] = "binary"
    #self.response.out.write(notfound)
    #http://localhost:8080/media/album/bed-and-breakfast-dove-siamo/random
      
application = webapp.WSGIApplication([('/media', Main),
                                                          ('/media/', Main),
                                                          #('/media/album/.*/.*/preview', Preview),
                                                          #('/media/album/.*/.*/thumbnail', Thumbnail),
                                                          #('/media/album/.*/.*', Download),
                                                          #('/media/album/.*', ShowCat),
                                                          ('/media/album/.*/random', CatRandomDownload),
                                                          ('/media/.*/preview', Preview),
                                                          ('/media/.*/thumbnail', Thumbnail),
                                                          ('/media/.*', Download)],
                                                            gpress.Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()