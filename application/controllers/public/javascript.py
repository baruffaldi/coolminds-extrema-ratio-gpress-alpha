#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds DC
@project: JavaScript Auto Compress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

#from application.utils.jscompress import JS
from application.utils.jscompress.jsmin import jsmin

class Request(webapp.RequestHandler):
  def is_js(self, real_path):
    return True if real_path.split(".")[-1] == "js" else False
    
  def get(self):
    requested_path = "/".join([x for x in os.environ["PATH_INFO"].split("/")[2:]])
    real_path = "../../static/javascript/%s" % requested_path
    try:
      script = open(real_path, "r").read()
    except:
      self.error(404)
      return

    self.response.out.write(
      script if Debug and not self.is_js(real_path) else jsmin(script)
    )
    
class Forbidden(webapp.RequestHandler):
  def get(self):
    self.error(403)

Debug = True if os.environ["REMOTE_ADDR"] == "127.0.0.1" else False

application = webapp.WSGIApplication(
                   [('/javascript/.*', Request),
                    ('/.*', Forbidden)], 
                   debug=Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()