#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode

from application.models import news 
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

News = news.News

class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    import datetime
    postz = News.all().filter('publish_status >=', 1).order('-publish_status').order('-publish_date')
    postz.fetch(int(gp.site.news_limit), tools.get_offset())
    
    tools.render(self, gp, 'news', {'news': postz})


class ReadNews(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    key = tools.get_url_var('news')
    
    try: post = News.get(key)
    except:
      article = News.all()
      article.filter('permalink =', key)
      if not gp.user.is_contributor():
        article.filter('publish_status >=', 1)
      if article.count(1):
        article = article.fetch(1)[0]
      else:
        article = False
        
    if article:
      if not article.publish_status and not gp.user.is_contributor():
          self.redirect('/errors/forbiddens?%s' %
                 urlencode({'ref': self.request.uri}), True)
          return
          
      gp.page.process_page(article)
      tools.render(self, gp, 'news-article', {'article': article})
    else:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)


application = webapp.WSGIApplication([('/news', Main),
                                                          ('/news/', Main),
                                                          ('/news/.*', ReadNews)],
                                                          gpress.Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()