#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.models import links
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

Link = links.Link
TableVars = ["delete","set_public","set_private"]

class MainPage(webapp.RequestHandler):
	def get_data(self):
		return Link.all().order('-language').order('-update_date').fetch(100)
	
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		tools.render(self, gp, 'table', {'links': self.get_data()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([unicode(x) for x in Link.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in list(self.request.POST)])).split(",")
		while '' in keys:
			keys.remove('')
		while keys.count('all_entities'):
			keys.remove('all_entities')
		while keys.count('all_entities_'):
			keys.remove('all_entities_')
		
		try:
			if self.request.get('delete', default_value=False):
				keys.remove('delete')
				db.delete(keys)
			elif self.request.get('delete_categories', default_value=False):
				keys.remove('delete_categories')
				contents = Link.all()
				for key in keys:
					contents.filter('categories_keys IN', [key])
					
				for c in contents.fetch(1000):
					for key in keys:
						c.categories_keys.remove(key)
						c.save()
						
				db.delete(keys)
			else:
				for key in keys:
					if key not in TableVars:
						link = Link.get(db.Key(key))
						if link:
							#if self.request.get('delete', default_value=False):
								#link.delete()
								#link.public = -2
							if self.request.get('set_public', default_value=False):
								link.public = 1
							elif self.request.get('set_private', default_value=False):
								link.public = 0
							elif self.request.get('set_draft', default_value=False):
								link.public = -1
							link.save()
		except Exception, e:
			gp.error = e
			gp.actionstatus = -1

		tools.render(self, gp, 'table', {'links': self.get_data()})


class Add(webapp.RequestHandler):
	def get_images(self):
		from application.models import media
		return media.Media.all().order('title').fetch(1000)
		
	def get_data(self, key):
		return Link.get(db.Key(key))
		
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		if gp.action == "update":
			gp.page.title = "Update link details"
			link = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Add a link"
			link = {}

		tools.render(self, gp, 'form', {'link': link, 'media': self.get_images()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		link = []
		
		if self.request.get('key', default_value=False):
			gp.page.title = "Update link"
			try:
				link = db.get(db.Key(self.request.get('key')))
				# Update
				if link.protection <= 1 or link.user == gp.user_instance or gp.user.is_developer():
					link.update(self.request.POST)
					# Save
					link.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
		else:
			gp.page.title = "Add new link"
			try:
				link = Link(url=self.request.get('url'))
				if self.request.get('permalink', default_value=False):
					link.permalink = self.request.get('permalink')
				else: 
					key_name = self.request.get('title', default_value=False)
					if not key_name:
						key_name = self.request.get('url')
					link.permalink = tools.key_name(key_name , token=True)
				
				if gp.has_key("newLanguage") and gp.newLanguage:
					link.main = Link.all().filter('permalink =', file.permalink).filter('main !=', None).fetch(1)[0]
				link.update(self.request.POST)
				link.put()
				self.redirect("/gp-admin/links", False)
				return
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				link = dict(self.request.POST)
						
		tools.render(self, gp, 'form', {'link': link, 'media': self.get_images()})


class UpdateSharing(webapp.RequestHandler):
	def get_images(self):
		from application.models import media
		return media.Media.all().order('title').fetch(1000)
	
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		link = []
		if self.request.get('key', default_value=False):
			gp.page.title = "Update link"
			try:
				link = db.get(db.Key(self.request.get('key')))
				# Update
				if link.protection <= 1 or link.user == gp.user_instance or gp.user.is_developer():
					link.update_sharing(self.request.POST)
					# Save
					link.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				
			tools.render(self, gp, 'form', {'link': link, 'media': self.get_images()})
			
class Delete(webapp.RequestHandler):
	def get(self):
		gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([str(x) for x in Link.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in tools.get_requested_object_list(3)])).split(",")
		
		while '' in keys:
			keys.remove('')
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/links/add', Add),
																													('/gp-admin/links/update/.*/sharing', UpdateSharing),
									  																		  ('/gp-admin/links/update/.*', Add),
									  									  									('/gp-admin/links/delete/.*', Delete),
                                                          ('/gp-admin/links.*', MainPage)],
                                                          gpress.Debug)

def main():
	run_wsgi_app(application)

if __name__ == "__main__":
	main()