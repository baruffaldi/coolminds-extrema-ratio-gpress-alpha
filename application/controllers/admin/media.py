#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os, mimetypes
mimetypes.add_type('video/3gpp', '3gp')

from application.models import media
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

Media = media.Media


class MainPage(webapp.RequestHandler):
	def get_data(self):
		return Media.all().order('-update_date').fetch(100)
	
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		tools.render(self, gp, 'table', {'medias': self.get_data()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([unicode(x) for x in Media.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in list(self.request.POST)])).split(",")
		while '' in keys:
			keys.remove('')
		while keys.count('all_entities'):
			keys.remove('all_entities')
		while keys.count('all_entities_'):
			keys.remove('all_entities_')

		if self.request.get('delete', default_value=False):
			keys.remove('delete')
			from application.models import links, news
			for key in keys:
				if key not in ["delete","set_public","set_private","set_draft","all_entities"]:
					ls = links.Link.all().filter('image', db.Key(key.strip())).fetch(1000)
					ls.extend(news.News.all().filter('image', db.Key(key.strip())).fetch(1000))
					for link in ls:
						link.image = None
						link.save()
			db.delete(keys)
		elif self.request.get('delete_albums', default_value=False):
			keys.remove('delete_albums')
			contents = Media.all()
			for key in keys:
				contents.filter('albums_keys IN', [key])
				
			for c in contents.fetch(1000):
				for key in keys:
					c.albums_keys.remove(key)
					c.save()
					
			db.delete(keys)
		else:
			for key in keys:
				if key not in ["delete","set_public","set_private","set_draft"]:
					try:
						media = db.get(db.Key(key))
						if media:
							#if self.request.get('delete', default_value=False):
								#media.delete()
								#media.public = -2
							if self.request.get('set_public', default_value=False):
								media.public = 1
							elif self.request.get('set_private', default_value=False):
								media.public = 0
							elif self.request.get('set_draft', default_value=False):
								media.public = -1
							media.save()
					except Exception, e:
						gp.error = e
						gp.actionstatus = -1
						
		tools.render(self, gp, 'table', {'medias': self.get_data()})

		
class Add(webapp.RequestHandler):	
	def get_data(self, key):
		return Media.get(db.Key(key))

	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)

		if gp.action == "update":
			gp.page.title = "Update media details"
			media = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Upload a media"
			media = {}

		from application.gpress.mimetypes import mimetypes as m
		tools.render(self, gp, 'form', {'media': media, 'mimetypes': m})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		
		if self.request.get('key', default_value=False):
			gp.page.title = "Update media details"
			""" Update entry """
			try:
				media = db.get(db.Key(self.request.get('key')))
				# Update
				if media.protection <= 1 or media.user == gp.user_instance or gp.user.is_developer():
					media.update(self.request.POST)
				if self.request.get('stream', default_value=False):
					media.stream=db.Blob(self.request.POST.get('stream').file.read())
					media.filename = self.request.POST.get('stream').filename
					media.mimetype = mimetypes.guess_type(media.filename)[0]
					media.size = len(media.stream)
					# Save
					media.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
		else:
			gp.page.title = "Add new media"
			""" Add media to db """

			media = Media()
			media.update(self.request.POST)
			
			if self.request.get('permalink', default_value=False):
				media.permalink = self.request.get('permalink')
			else: 
				key_name = self.request.get('title', default_value=False)
				if not key_name:
					key_name = self.request.POST.get('stream').filename
				media.permalink = tools.key_name(key_name , token=True)
				
			if self.request.get('stream', default_value=False):
				media.stream=db.Blob(self.request.POST.get('stream').file.read())
				media.filename = self.request.POST.get('stream').filename
				media.mimetype = mimetypes.guess_type(media.filename)[0]
				media.size = len(media.stream)
			elif gp.has_key("newLanguage") and gp.newLanguage:
				media.main = Media.all().filter('permalink =', file.permalink).filter('main !=', None).fetch(1)[0]

			media.put()
			self.redirect("/gp-admin/media/update/%s" % media.key(), False)
			return

		from application.gpress.mimetypes import mimetypes as m
		tools.render(self, gp, 'form', {'media': media, 'mimetypes': m})
			

class UpdateSharing(webapp.RequestHandler):
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		media = []
		if self.request.get('key', default_value=False):
			gp.page.title = "Update media"
			try:
				media = db.get(db.Key(self.request.get('key')))
				# Update
				if media.protection <= 1 or media.user == gp.user_instance or gp.user.is_developer():
					media.update_sharing(self.request.POST)
					# Save
					media.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				
			tools.render(self, gp, 'form', {'media': media})


class Delete(webapp.RequestHandler):
	def get(self):
		from application.models import links, news
		
		gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([str(x) for x in Media.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in tools.get_requested_object_list(3)])).split(",")
		
		while '' in keys:
			keys.remove('')
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')

		for key in keys:
			ls = links.Link.all().filter('image', db.Key(key.strip())).fetch(1000)
			ls.extend(news.News.all().filter('image', db.Key(key.strip())).fetch(1000))
			for link in ls:
				link.image = None
				link.save()
				
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/media/add', Add),
																													('/gp-admin/media/update/.*/sharing', UpdateSharing),
																													('/gp-admin/media/update/.*', Add),
																													('/gp-admin/media/delete/.*', Delete),
																													('/gp-admin/media.*', MainPage)],
																													gpress.Debug)

def main():
	run_wsgi_app(application)

if __name__ == "__main__":
	main()