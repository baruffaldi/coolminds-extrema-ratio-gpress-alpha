#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.models import files, links, media, pages, posts
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

Post = posts.Post
TableVars = ["delete","set_public","set_private"]


class MainPost(webapp.RequestHandler):
	def get_data(self):
		return Post.all().order('-language').order('-publish_date').order('-update_date').fetch(100)
	
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		tools.render(self, gp, 'table', {'posts': self.get_data()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([unicode(x) for x in Post.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in list(self.request.POST)])).split(",")
		while '' in keys:
			keys.remove('')
		while keys.count('all_entities'):
			keys.remove('all_entities')
		while keys.count('all_entities_'):
			keys.remove('all_entities_')

		try:
			if self.request.get('delete', default_value=False):
				keys.remove('delete')
				db.delete(keys)
			elif self.request.get('delete_categories', default_value=False):
				keys.remove('delete_categories')
				contents = Post.all()
				for key in keys:
					contents.filter('categories_keys IN', [key])
					
				for c in contents.fetch(1000):
					for key in keys:
						c.categories_keys.remove(key)
						c.save()
						
				db.delete(keys)
			else:
				for key in keys:
					if key not in TableVars:
						post = Post.get(db.Key(key))
						if post:
							#if self.request.get('delete', default_value=False):
								#post.delete()
								#post.public = -2
							if self.request.get('set_public', default_value=False):
								post.public = 1
							elif self.request.get('set_private', default_value=False):
								post.public = 0
							elif self.request.get('set_draft', default_value=False):
								post.public = -1
							post.save()
		except Exception, e:
			gp.error = e
			gp.actionstatus = -1

		tools.render(self, gp, 'table', {'posts': self.get_data()})

		
class Add(webapp.RequestHandler):
	def get_data(self, key):
		return Post.get(db.Key(key))
	
	def get_contents(self):
		return {
						'posts': posts.Post.all().filter('public >', -1).fetch(1000),
						'pages': pages.Page.all().filter('public >', -1).fetch(1000),
						'media': media.Media.all().filter('public >', -1).fetch(1000),
						'files': files.File.all().filter('public >', -1).fetch(1000),
						'links': links.Link.all().filter('public >', -1).fetch(1000)
						}
		
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		if gp.action == "update":
			gp.page.title = "Update post details"
			post = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Write new post"
			post = {}
		
		tools.render(self, gp, 'form', {'post': post, 'contents': self.get_contents()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		if self.request.get('key', default_value=False):
			gp.page.title = "Update post details"
			try:
				post = db.get(db.Key(self.request.get('key')))
				# Update
				if post.protection <= 1 or post.user == gp.user_instance or gp.user.is_developer():
					if post.publish_status <= 0 and self.request.get('publish_status', default_value=False) == "1":
						post.publish_user_key = str(gp.user.key())
					post.update(self.request.POST)
					# Save
					post.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				post = {}
		else:
			try:
				post = Post()
				post.update(self.request.POST)
				if self.request.get('permalink', default_value=False):
					post.permalink = self.request.get('permalink')
				else:
					key_name = self.request.get('title', default_value=False)
					if not key_name:
						key_name = self.request.get('description')
					post.permalink = tools.key_name(key_name , token=True)
				if self.request.get('publish_status', default_value=False) == "1":
					post.publish_user_key = str(gp.user.key())
				if gp.has_key("newLanguage"):
					post.main = Post.all().filter('permalink =', file.permalink).filter('main !=', None).fetch(1)[0]
				post.put()
				self.redirect("/gp-admin/posts/update/%s" % post.key(), False)
				return
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				post = dict(self.request.POST)

		tools.render(self, gp, 'form', {'post': post, 'contents': self.get_contents()})
			

class UpdateSharing(webapp.RequestHandler):
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		post = []
		if self.request.get('key', default_value=False):
			gp.post.title = "Update post"
			try:
				post = db.get(db.Key(self.request.get('key')))
				# Update
				if post.protection <= 1 or post.user == gp.user_instance or gp.user.is_developer():
					post.update_sharing(self.request.POST)
					# Save
					post.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				
			tools.render(self, gp, 'form', {'post': post})

		
class UpdateAttachments(webapp.RequestHandler):
	def post(self):
		gpress.Registry(self, gpress.CONTRIBUTOR)
		
		try:
			post = db.get(db.Key(self.request.get('key')))
			if not post.attachments_keys:
				post.attachments_keys = []
				
			if 'attach' in self.request.POST:
				keys = []
				keys.extend(self.request.get('posts', allow_multiple=True, default_value=[]))
				keys.extend(self.request.get('pages', allow_multiple=True, default_value=[]))
				keys.extend(self.request.get('media', allow_multiple=True, default_value=[]))
				keys.extend(self.request.get('files', allow_multiple=True, default_value=[]))
				keys.extend(self.request.get('links', allow_multiple=True, default_value=[]))
				for key in keys:
					if key not in post.attachments_keys:
						post.attachments_keys.append(key)

			elif 'delete' in self.request.POST:
				keys = list(self.request.POST)
				keys.remove('delete')
				for key in keys:
					if key in post.attachments_keys:
						post.attachments_keys.remove(key)
				
			post.save()
		except:
			""
			
		self.redirect(os.environ["HTTP_REFERER"], False)

class Delete(webapp.RequestHandler):
	def get(self):
		gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([str(x) for x in Post.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in tools.get_requested_object_list(3)])).split(",")
		
		while '' in keys:
			keys.remove('')
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/posts/add', Add),
																													('/gp-admin/posts/update/.*/sharing', UpdateSharing),
																													('/gp-admin/posts/update/.*/attachments', UpdateAttachments),
																													('/gp-admin/posts/update/.*', Add),
																													('/gp-admin/posts/delete/.*', Delete),
																													('/gp-admin/posts.*', MainPost)],
																													gpress.Debug)

def main():
	run_wsgi_app(application)

if __name__ == "__main__":
	main()