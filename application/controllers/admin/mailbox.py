#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.models import mailbox
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

from urllib import urlencode

Message = mailbox.Message

class List(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.CONTRIBUTOR)
    tools.render(self, gp, 'table', {'messages': gp.user.messages()})
    
  def post(self):
    gp = gpress.Registry(self, gpress.CONTRIBUTOR)
    keys = list(self.request.POST)
    while '' in keys:
      keys.remove('')
    while keys.count('all_entities'):
      keys.remove('all_entities')
    while keys.count('all_entities_'):
      keys.remove('all_entities_')
    
    try:
      if self.request.get('delete', default_value=False):
        keys.remove('delete')
        db.delete(keys)
      elif self.request.get('delete_categories', default_value=False):
        keys.remove('delete_categories')
        contents = Message.all()
        for key in keys:
          contents.filter('categories_keys IN', [key])
          
        for c in contents.fetch(1000):
          for key in keys:
            c.categories_keys.remove(key)
            c.save()
            
        db.delete(keys)
    except:
      ""
    self.redirect('/gp-admin/mailbox', True)

class Write(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(
                         handler=self,
                         authrank=gpress.CONTRIBUTOR,
                         privacy=1)
    tools.parseHandler(self, gp)
    
    tools.render(self, gp, 'messageform', {})
    
  def post(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    sent = 1
    try:
      message = mailbox.Message(
            name=self.request.get('name', default_value=gp.user.name),
            reference=self.request.get('reference', default_value=gp.user.email),
            body=self.request.get('body', default_value="")
      )
      if self.request.get('destinations', default_value=False):
        message.destinations=[str(self.request.get('destinations')).strip()],
      message.put()
    except:
      sent = 0
    
    if sent: self.redirect('/bed-and-breakfast-home', False)
    else:
      self.redirect("%s?sent=%i" % (os.environ["HTTP_REFERER"], sent), False)


class Read(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    
    try:
      message = mailbox.Message.get(tools.get_url_var('mailbox'))
      message.read = True
      message.save()
    except:
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)
      return

    tools.render(self, gp, 'message', {'message': message})
      
class Delete(webapp.RequestHandler):
  def get(self):
    gpress.Registry(self, gpress.USER)
    keys = tools.get_requested_object_list(3)
    
    while '' in keys:
      keys.remove('')
    if 'delete' in keys:
      keys.remove('delete')
    if 'all_entities' in keys:
      keys.remove('all_entities')
    db.delete(keys)
    
    self.redirect(os.environ["HTTP_REFERER"], False)

Default = List
Send = Write
RoutingTable = [('/gp-admin/mailbox/send', Write),
                        ('/gp-admin/mailbox/list', Default),
                        ('/gp-admin/mailbox/.*/delete', Delete),
                        ('/gp-admin/mailbox/.*', Read),
                        ('/gp-admin/mailbox.*', Default),
                        ('/mailbox/send', Write),
                        ('/mailbox.*', Default)]

application = webapp.WSGIApplication(RoutingTable, gpress.Debug)



def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()