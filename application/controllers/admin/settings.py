#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode

from application.models import settings, pages, posts, news
from application.gpress import tools
from application import gpress

from google.appengine.api import memcache
from google.appengine.ext import webapp, db
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app


class MainPage(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self, gpress.ADMINISTRATOR)
		
		contents = {
			'news': news.News.all().filter('publish_status >=', 0).fetch(1000),
			'pages': pages.Page.all().filter('publish_status >=', 0).fetch(1000),
			'posts': posts.Post.all().filter('publish_status >=', 0).fetch(1000)
		} 

		tools.render(self, gp, 'form', {'contents': contents})
		
	def post(self):
		memcache.delete('gp_settings')
		gp = gpress.Registry(self, gpress.ADMINISTRATOR)
		
		db.delete(settings.Settings.all().fetch(1000))
		
		new_settings = []
		for name in self.request.POST:
			if name not in ['', 'submit']:
				value = self.request.get(name, default_value=False).strip()
				if value:
					new_settings.append(settings.Settings(field=str(name),value=str(value)))
					
		db.put(new_settings)
		
		gp.page.changed = True
		contents = {
			'news': news.News.all().filter('publish_status >=', 0).fetch(1000),
			'pages': pages.Page.all().filter('publish_status >=', 0).fetch(1000),
			'posts': posts.Post.all().filter('publish_status >=', 0).fetch(1000)
		} 
			
		self.redirect('/gp-admin/settings', False)
		
		
class Robots(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry()
		if not gp.user.is_admin:
			self.redirect('/gp-admin/errors/forbidden?%s' %
                   urlencode({'ref': self.request.uri}), True)
			return
		
		gp.page.changed = False
		gp.page.title = "Robots"
		gp.page.url = "settings"
		gp.page.description = "From this page you can manage the robots access."
		
		# Parse the robots.txt
		robots = {}
		gp.page.body = template.render('../../views/settings-robots.html', {'site': gp.settings, 'gpress': robots})
		
		self.response.out.write(
			template.render('../../views/template.html', gp.get_template_vars())
		)
		
	def post(self):
		# Save robots.txt file
		
		gp = gpress.Registry()
		if not gp.user.is_admin:
			self.redirect('/gp-admin/errors/forbidden?%s' %
                   urlencode({'ref': self.request.uri}), True)
			return
		
		gp.page.changed = True
		gp.page.title = "Robots"
		gp.page.url = "settings"
		gp.page.description = "From this page you can manage the robots access."
		
		# Parse the robots.txt
		robots = {}
		gp.page.body = template.render('../../views/settings-robots.html', {'site': gp.settings, 'gpress': robots})
		
		self.response.out.write(
			template.render('../../views/template.html', gp.get_template_vars())
		)


application = webapp.WSGIApplication([('/gp-admin/settings.*', MainPage),
									  ('/gp-admin/settings/robots', Robots)],
                                     gpress.Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()