#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.models import contacts
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

Contact = contacts.Contact
TableVars = ["delete","set_public","set_private"]

class MainPage(webapp.RequestHandler):
	def get_data(self):
		return Contact.all().fetch(100, tools.get_offset())
	
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		tools.render(self, gp, 'table', {'contacts': self.get_data()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		keys = list(self.request.POST)
		while keys.count('all_entities'):
			keys.remove('all_entities')
		
		try:
			if self.request.get('delete', default_value=False):
				keys.remove('delete')
				db.delete(keys)
			else:
				for key in keys:
					if key not in TableVars:
						contact = Contact.get(db.Key(key))
						if contact:
							#if self.request.get('delete', default_value=False):
								#contact.delete()
								#contact.public = -2
							if self.request.get('set_public', default_value=False):
								contact.public = 1
							elif self.request.get('set_private', default_value=False):
								contact.public = 0
							elif self.request.get('set_draft', default_value=False):
								contact.public = -1
							contact.save()
		except Exception, e:
			gp.error = e
			gp.actionstatus = -1

		tools.render(self, gp, 'table', {'contacts': self.get_data()})


class Add(webapp.RequestHandler):
	def get_images(self):
		from application.models import media
		return media.Media.all().order('title').fetch(1000)
		
	def get_data(self, key):
		return Contact.get(db.Key(key))
		
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		if gp.action == "update":
			gp.page.title = "Update contact details"
			contact = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Add a contact"
			contact = {}

		tools.render(self, gp, 'form', {'contact': contact, 'media': self.get_images()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True

		if self.request.get('key', default_value=False):
			gp.page.title = "Update contact"
			try:
				contact = Contact.get(self.request.get('key'))
				contact.update(self.request.POST)
				contact.save()
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				contact = dict(self.request.POST)
		else:
			gp.page.title = "Add new contact"
			try:
				contact = Contact(url=self.request.get('url'))
				
				key_name = self.request.get('title', default_value=False)
				if not key_name:
					key_name = self.request.get('url')
				contact.permacontact = tools.key_name(key_name, token=True)
				contact.update(self.request.POST)
				contact.put()
				self.redirect("/gp-admin/contacts", False)
				return
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				contact = dict(self.request.POST)
						
		tools.render(self, gp, 'form', {'contact': contact, 'media': self.get_images()})
			
		
class Delete(webapp.RequestHandler):
	def get(self):
		gpress.Registry(self, gpress.CONTRIBUTOR)
		
		keys = tools.get_requested_object_list(3)
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/contacts/add', Add),
									  ('/gp-admin/contacts/update/.*', Add),
									  ('/gp-admin/contacts/delete/.*', Delete),
                                      ('/gp-admin/contacts.*', MainPage)],
                                     gpress.Debug)

def main():
	run_wsgi_app(application)
  
if __name__ == "__main__":
	main()