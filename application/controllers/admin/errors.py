#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp
from google.appengine.api import users
from google.appengine.ext.webapp.util import run_wsgi_app


class ErrorHandler(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
	
		error = {'type': "warning",
			 'code': "500",
			 'title': "Ooops, something goes wrong."}
		
		if gpress.Debug:
			error['details'] = {'referer': self.request.get('ref', default_value=None)}
			 
		tools.render_error(self, None, {'error':error})
		
	
class ForbiddenHandler(webapp.RequestHandler):
	def get(self):
		if not users.get_current_user():
			self.redirect(
				users.create_login_url(
					self.request.get(
						'ref', 
						default=self.request.headers["REFERER"])
				), False)
			return
	
		error = {'type': "notice",
			 'code': "403",
			 'title': "Forbidden",
			 'description': "you're not authorized to view the page you requested"}
		
		if gpress.Debug:
			error['details'] = {'referer': self.request.get('ref', default_value=None)} 
			
		tools.render_error(self, None, error)

	
class DocumentNotFoundHandler(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self, gpress.ADMINISTRATOR)
		
		error = {'type': "info",
			 'code': "404",
			 'title': "Page or document not found",
			 'description': """<pre>
It is said, "To err is human"
That quote from alt.times.lore,
Alas, you have made an error,
So I say, "404."

Double-check your URL,
As we all have heard before.
You ask for an invalid filename,
And I respond, "404."

Perhaps you made a typo --
Your fingers may be sore --
But until you type it right,
You'll only get 404.

Maybe you followed a bad link,
Surfing a foreign shore;
You'll just have to tell that author
About this 404.

I'm just a lowly server
(Who likes to speak in metaphor),
So for a request that Idon't know,
I must return 404.

Be glad I'm not an old mainframe
That might just dump its core,
Because then you'd get a ten-meg file
Instead of this 404.

I really would like to help you,
But I don't know what you're looking for,
And since I don't know what you want,
I give you 404.

Remember Poe, insane with longing
For his tragically lost Lenore.
Instead, you quest for files.
Quoth the Raven, "404!"
		</pre>"""}
		
		if gpress.Debug:
			error['details'] = {'referer': self.request.get('ref', default_value=None)} 
			
		tools.render_error(self, None, {'error':error})


application = webapp.WSGIApplication([('/gp-admin/errors/forbidden', ForbiddenHandler),
																													('/gp-admin/errors/document-not-found', DocumentNotFoundHandler),
																													('/.*', ErrorHandler)],
																													gpress.Debug)

def main():
	run_wsgi_app(application)

if __name__ == "__main__":
	main()