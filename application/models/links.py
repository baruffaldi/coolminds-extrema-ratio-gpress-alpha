#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
									'title',
									'permalink'
									]

skip_fields = [
							 'stream',
							 'key'
							 ]

from datetime import datetime
from urllib import urlencode

from application.models import media

from application.gpress import tools
from application.gpress import models
from google.appengine.ext import db

Media = media.Media

class Link(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)

	""" Content Info """
	permalink = db.StringProperty()
	title = db.StringProperty()
	description = db.TextProperty()
	tags = db.StringListProperty(default=[])
	categories_keys = db.StringListProperty(default=[])
	
	""" Sharing """
	publish_status = db.IntegerProperty(default=-1)
	feed_shared = db.BooleanProperty(default=True)
	sitemap_shared = db.BooleanProperty(default=True)
	sitemap_priority = db.FloatProperty()
	sitemap_freq = db.StringProperty()
	protection = db.IntegerProperty(default=0)
	contributors_keys = db.StringListProperty(default=[])
	def contributors(self):
		ret = []
		from application.models.users import User
		for user in self.contributors_keys:
			ret.append(User.get_by_key_name(user))
		return ret
	
	""" Content Details """	
	main = db.SelfReferenceProperty()
	url = db.LinkProperty()
	image = db.ReferenceProperty(Media)
	counter = db.IntegerProperty(default=0)
	favicon_key = db.StringProperty()
	language = db.StringProperty()

	""" Utils """
	def favicon(self):
		if self.favicon_key:
			try:
				return Media.get(db.Key(self.favicon_key))
			except:
				return False
		
	def categories(self):
		return LinkCategory.get(",".join([db.Key(x) if x else db.Key() for x in self.categories_keys]).split(','))
	
	def languages_count(self):
		return Link.all().filter('permalink =', self.permalink).filter('language !=', self.language).count(1000)
	
	def languages(self):
		return Link.all().filter('permalink =', self.permalink).filter('language !=', self.language).fetch(1000)
					
	def get_kind(self):
		return 'Links'
	
	def update_sharing(self, values_dict={}):
		self.contributors_keys = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "contributors_keys" and value:
				self.contributors_keys.extend((",".join([str(x).strip() for x in value])).split(","))
				#self.contributors_keys.append(str(value))
				#self.contributors_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.contributors_keys.count(""):
					#self.contributors_keys.remove("")
				while self.contributors_keys.count(""):
					self.contributors_keys.remove("")
			
	def update(self, values_dict={}):
		self.categories_keys = []
		self.tags = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "publish_status":
				self.publish_status = int(value)
			elif name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "publish_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.publish_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "image":
				if value:
					self.image = db.Key(value)
				else:
					self.image = None
			elif name == "permalink":
				self.permalink = tools.key_name(str(value).strip())
			elif name == "categories_keys" and value:
				self.categories_keys.extend((",".join([str(x) for x in value])).split(","))
				#self.categories_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.categories_keys.count(""):
					#self.categories_keys.remove("")
			elif name == "tags":
				self.tags.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.tags.count(""):
					self.tags.remove("")
			elif name == "new_category" and value:
				cat_check = LinkCategory.all(keys_only=True).filter('title =', str(value).strip())
				if cat_check.count(1) and str(cat_check.fetch(1)[0]) not in self.categories_keys:
					self.categories_keys.append(str(cat_check.fetch(1)[0]))
				else:
					cat = LinkCategory(title=str(value).strip())
					cat.put()
					if cat.is_saved() and str(cat.key()) not in self.categories_keys:
						self.categories_keys.append(str(cat.key()))
						
			elif name not in skip_fields and name in self.properties().keys():
				if isinstance(self.__getattribute__(name), db.IntegerProperty):
					self.__setattr__(name, int(str(value).strip()))
				elif isinstance(self.__getattribute__(name), db.FloatProperty):
					self.__setattr__(name, float(str(value).strip()))
				else:
					self.__setattr__(name, str(value).strip())
				
	def view_url(self):
		return '/links/%s' % self.permalink
				
	def visit_url(self):
		return '/links/%s/redirect' % self.permalink
		
	def favicon_url(self):
		favicon = self.favicon()
		if favicon:
			return '/media/%s/download?width=64&amp;disable_counter=1' % favicon.permalink
		else:
			return "http://%s/favicon.ico" % str(self.url).split('/')[2]
	
	def code(self, width=200):
		if self.image:
			thumb_url = '/media/%s/thumbnail?width=%i&amp;disable_counter=1' % ( self.image.permalink, width )
		else:
			thumb_url = 'http://api.thumbalizr.com/?%s&amp;width=%s' % (urlencode({'url': self.url}), width)
		return "<img src='%s' alt='Preview Image' />" % thumb_url
	
	def thumbnail_url(self):
		if self.image:
			return '/media/%s/download?width=300&amp;disable_counter=1' % self.image.permalink
		else:
			return 'http://api.thumbalizr.com/?%s&amp;width=300' % urlencode({'url': self.url})
	
	def get_image(self):
		return Media.get(self.image.key())

class LinkCategory(db.Model):
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	title = db.CategoryProperty()
	
	def contents(self):
		return Link.all().filter('categories_keys IN', [str(self.key())]).fetch(1000)
	
	def contents_count(self):
		return Link.all().filter('categories_keys IN', [str(self.key())]).count(1000)

class LinkComment(db.Model):
		user = db.UserProperty(auto_current_user_add=True)
		date = db.DateTimeProperty(auto_now_add=True)
		
		content = db.ReferenceProperty(Link)
		body = db.TextProperty()

		status = db.IntegerProperty(default=-1)