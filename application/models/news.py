#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
								  'title',
								  'permalink'
								  ]

skip_fields = [
							 'stream',
							 'key'
							 ]

from application.gpress import tools
from application.gpress import models

from application.models import media
Media = media.Media

from datetime import datetime
from google.appengine.ext import db

class News(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	publish_date = db.DateTimeProperty()
	publish_user_key = db.StringProperty()
	def publish_user(self):
		from application.models.users import User
		try:
			return User.get(self.publish_user_key)
		except:
			""

	""" Content Info """
	permalink = db.StringProperty()
	title = db.StringProperty()
	body = db.TextProperty()
	tags = db.StringListProperty(default=[])

	""" Sharing """
	publish_status = db.IntegerProperty(default=-1)
	feed_shared = db.BooleanProperty(default=True)
	sitemap_shared = db.BooleanProperty(default=True)
	sitemap_priority = db.FloatProperty()
	sitemap_freq = db.StringProperty()
	protection = db.IntegerProperty(default=0)
	contributors_keys = db.StringListProperty(default=[])
	def contributors(self):
		ret = []
		from application.models.users import User
		for user in self.contributors_keys:
			ret.append(User.get_by_key_name(user))
		return ret
	
	""" Content Details """	
	main = db.SelfReferenceProperty()
	language = db.StringProperty()
	publish_status = db.IntegerProperty(default=-1)
	image = db.ReferenceProperty(Media)
	attachments_keys = db.StringListProperty(default=[])
		
	""" Utils """
	def attachments(self):
		from application.models import posts, pages, files, media, links
		return db.get(self.attachments_keys)
	
	def attachments_count(self):
		return len(self.attachments_keys)
	
	def languages_count(self):
		return News.all().filter('permalink =', self.permalink).filter('language !=', self.language).count(1000)
	
	def languages(self):
		return News.all().filter('permalink =', self.permalink).filter('language !=', self.language).fetch(1000)
	
	def get_kind(self):
		return 'News'
	
	def update_sharing(self, values_dict={}):
		self.contributors_keys = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "contributors_keys" and value:
				self.contributors_keys.extend((",".join([str(x).strip() for x in value])).split(","))
				#self.contributors_keys.append(str(value))
				#self.contributors_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.contributors_keys.count(""):
					#self.contributors_keys.remove("")
				while self.contributors_keys.count(""):
					self.contributors_keys.remove("")
		
	def update(self, values_dict={}):
		self.contributors_keys = []
		self.tags = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
				self.feeds_shared = bool(value)
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(value)
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "publish_status":
				self.publish_status = int(value)
			elif name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "publish_date":
				if value.strip():
					date = value.split(' ')
					year, month, day = date[0].split('-')
					hours, minutes, seconds = date[1].split(':')
					self.publish_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "image":
				if value:
					self.image = db.Key(value)
				else:
					self.image = None
			elif name == "permalink":
				self.permalink = tools.key_name(str(value).strip())
			elif name == "contributors_keys":
				self.contributors_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.contributors_keys.count(""):
					self.contributors_keys.remove("")
			elif name == "tags":
				self.tags.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.tags.count(""):
					self.tags.remove("")
			elif name not in skip_fields and name in self.properties().keys():
				if isinstance(self.__getattribute__(name), db.IntegerProperty):
					self.__setattr__(name, int(str(value).strip()))
				elif isinstance(self.__getattribute__(name), db.FloatProperty):
					self.__setattr__(name, float(str(value).strip()))
				else:
					self.__setattr__(name, str(value).strip())
					
		if self.publish_status < 0:
			self.publish_date = None
		if self.publish_status > 0 and not self.publish_date:
			self.publish_date = datetime.now()
			from google.appengine.api import users as gusers
			from application.models import users
			u = users.User.all().filter('email =', gusers.get_current_user().email())
			self.publish_user_key = str(u.fetch(1)[0].key()) if u.count(1) else None
			
	def all_public(self):
		return self.all().filter('publish_status >=', 1)
			
	def all_not_listed(self):
		return self.all().filter('publish_status =', 0)
			
	def all_accessible(self):
		return self.all().filter('publish_status >=', 0)
				
	def view_url(self):
		return '/news/%s' % self.permalink
				
	def image_url(self, width=200):
		if self.image:
			return '%s?width=%i&amp;disable_counter=1' % ( self.image.thumbnail_url(), width )
	
	def code(self, width=200):
		if self.image:
			thumb_url = '%s?width=%i&amp;disable_counter=1' % ( self.image.thumbnail_url(), width )
			return "<img src='%s' alt='Preview Image' />" % thumb_url