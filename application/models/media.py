#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
								  'title',
								  'permalink'
								  ]

skip_fields = [
							 'stream',
							 'key'
							 ]

from datetime import datetime
from urllib import urlencode

from application.gpress import tools
from application.gpress import models
from google.appengine.ext import db

class Media(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	""" Content Info """
	permalink = db.StringProperty()
	title = db.StringProperty()
	description = db.TextProperty(indexed=False)
	tags = db.StringListProperty(default=[])
	albums_keys = db.StringListProperty(default=[])

	""" Sharing """
	publish_status = db.IntegerProperty(default=-1)
	feed_shared = db.BooleanProperty(default=True)
	sitemap_shared = db.BooleanProperty(default=True)
	sitemap_priority = db.FloatProperty()
	sitemap_freq = db.StringProperty()
	protection = db.IntegerProperty(default=0)
	contributors_keys = db.StringListProperty(default=[])
	def contributors(self):
		ret = []
		from application.models.users import User
		for user in self.contributors_keys:
			ret.append(User.get_by_key_name(user))
		return ret
	
	""" Content Details """	
	main = db.SelfReferenceProperty()
	filename = db.StringProperty()
	mimetype = db.StringProperty(default="multipart/form-data")
	size = db.IntegerProperty(default=0)
	stream = db.BlobProperty()
	counter = db.IntegerProperty(default=0)
	language = db.StringProperty()
		
	""" Utils """
	def get_kind(self):
		return 'Media'
		
	def albums(self):
		return MediaAlbum.get([db.Key(x) for x in self.albums_keys])
	
	def languages_count(self):
		return Media.all().filter('permalink =', self.permalink).filter('language !=', self.language).count(1000)
	
	def languages(self):
		return Media.all().filter('permalink =', self.permalink).filter('language !=', self.language).fetch(1000)
		
	def update_albums(self, alb=[], new=None):
		self.albums_keys = alb
		if new:
			cat_check = MediaAlbum.all(keys_only=True).filter('title =', new)
			if cat_check.count(1) and str(cat_check.fetch(1)[0]) not in self.albums_keys:
				self.albums_keys.append(str(cat_check.fetch(1)[0]))
			else:
				cat = MediaAlbum(title=new)
				cat.put()
				if cat.is_saved() and str(cat.key()) not in self.albums_keys:
					self.albums_keys.append(str(cat.key()))
	
	def update_sharing(self, values_dict={}):
		self.contributors_keys = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "contributors_keys" and value:
				self.contributors_keys.extend((",".join([str(x).strip() for x in value])).split(","))
				#self.contributors_keys.append(str(value))
				#self.contributors_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.contributors_keys.count(""):
					#self.contributors_keys.remove("")
				while self.contributors_keys.count(""):
					self.contributors_keys.remove("")
		
	def update(self, values_dict={}):
		self.albums_keys = []
		self.contributors_keys = []
		self.tags = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name != 'albums_keys':
				value = str(value[0])
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "publish_status":
				self.publish_status = int(value)
			elif name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "publish_date":
				if value.strip():
					date = value.split(' ')
					year, month, day = date[0].split('-')
					hours, minutes, seconds = date[1].split(':')
					self.publish_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "filename":
				self.filename = "%s.%s" % ( value, self.extension() )
			elif name == "permalink":
				self.permalink = tools.key_name(str(value).strip())
			elif name == "albums_keys" and value:
				self.albums_keys.extend((",".join([str(x) for x in value])).split(","))
				#self.albums_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.albums_keys.count(""):
					#self.albums_keys.remove("")
			elif name == "tags":
				self.tags.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.tags.count(""):
					self.tags.remove("")
			elif name == "new_album" and value:
				cat_check = MediaAlbum.all(keys_only=True).filter('title =', str(value).strip())
				if cat_check.count(1) and str(cat_check.fetch(1)[0]) not in self.albums_keys:
					self.albums_keys.append(str(cat_check.fetch(1)[0]))
				else:
					cat = MediaAlbum(title=str(value).strip())
					cat.put()
					if cat.is_saved() and str(cat.key()) not in self.albums_keys:
						self.albums_keys.append(str(cat.key()))
						
			elif name not in skip_fields and name in self.properties().keys():
				if isinstance(self.__getattribute__(name), db.IntegerProperty):
					self.__setattr__(name, int(str(value).strip()))
				elif isinstance(self.__getattribute__(name), db.FloatProperty):
					self.__setattr__(name, float(str(value).strip()))
				else:
					self.__setattr__(name, str(value).strip())
				
	def view_url(self):
		return '/media/%s/preview' % self.permalink
				
	def download_url(self):
		return '/media/%s' % self.permalink
				
	def thumbnail_url(self):
		if self.mime() in ['image']:
			return '/media/%s/thumbnail' % self.permalink
		
		return False
				
	def filename_without_extension(self):
		if self.filename:
			return ".".join([ x for x in str(self.filename).split('.')[:1]])
				
	def extension(self):
		if self.filename:
			return str(str(self.filename).split('.')[-1]).strip()
	
	def mime(self):
		if self.mimetype:
			return str(str(self.mimetype).split('/')[0]).strip()
	
	def type(self):
		if self.mimetype:
			return str(str(self.mimetype).split('/')[1]).strip()
	
	def mimetype_image(self):
		return "/gp-admin/images/icons/mimetype/gpress-icon-mimetype-%s.png" % str(self.mimetype).split("/")[0]
	
	def code(self, width=200):
		if str(self.mime()).lower() == 'image':
			thumb_url = '%s?width=%i&amp;disable_counter=1' % ( self.download_url, width )
		else:
			thumb_url = 'http://api.thumbalizr.com/?%s&amp;width=%s' % (urlencode({'url': self.url}), width)
		return "<img style='width: 200px;' src='%s' alt='Preview Image' />" % thumb_url
	
	def comments(self):
		return MediaComment.all().filter('content =', self.key()).fetch(1000)
	
	def comments_counter(self):
		return MediaComment.all().filter('content =', self.key()).count(1000)

class MediaAlbum(db.Model):
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	title = db.CategoryProperty()
	
	def contents(self):
		return Media.all().filter('albums_keys IN', [str(self.key())]).fetch(1000)
	
	def contents_count(self):
		return Media.all().filter('albums_keys IN', [str(self.key())]).count(1000)

class MediaComment(db.Model):
		user = db.UserProperty(auto_current_user_add=True)
		date = db.DateTimeProperty(auto_now_add=True)
		
		content = db.ReferenceProperty(Media)
		body = db.TextProperty(indexed=False)

		status = db.IntegerProperty(default=-1)