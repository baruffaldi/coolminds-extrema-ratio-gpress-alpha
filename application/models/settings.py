#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from google.appengine.ext import db

class Settings(db.Model):
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True, indexed=False)
  update_date = db.DateTimeProperty(auto_now=True, indexed=False)
  update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
  
  field = db.StringProperty(required=True)
  value = db.StringProperty()
    
  def get_kind(self):
    return 'Setting'