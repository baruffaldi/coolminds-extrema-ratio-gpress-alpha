#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds DN
@project: Image-Tools
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.utils.jscompress import JS

from google.appengine.ext import webapp

register = webapp.template.create_template_register()

@register.filter
def jscompress(script):
	return JS({'js_code': str(script)}).compress()
jscompress.is_safe = True

@register.tag
def jscompresstag(parser, token):
	tag_name, varname, value = token.split_contents()
	print tag_name
	print varname
	print value
	return tag_name