'''DeviceAtlas exceptions'''

__version__ = '$Rev$' 
__author__ = 'Ronan Cremin <rcremin@mtld.mobi>'
__url__ = 'http://deviceatlas.com'
__license__ = '''Copyright (c) 2008, mTLD (dotMobi), All rights reserved.
Portions copyright (c) 2008 by Argo Interactive Limited.
Portions copyright (c) 2008 by Nokia Inc.
Portions copyright (c) 2008 by Telecom Italia Mobile S.p.A.
Portions copyright (c) 2008 by Volantis Systems Limited.
Portions copyright (c) 2002-2008 by Andreas Staeding.
Portions copyright (c) 2008 by Zandan.
'''

class Error(Exception):
    '''Base exception class'''
    pass


class InvalidPropertyException(Error):
    '''The InvalidPropertyException is thrown by the Api class.
    When there is an attempt to fetch a property that is unknown 
    for the supplied user agent.
    '''
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message


class UnkownPropertyException(Error):
    '''The UnknownPropertyException is thrown by the Api class.
    When there is an attempt to fetch a property unknown to the tree. 
    '''
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message


class IncorrectPropertyTypeException(Error):
    '''The IncorrectPropertyTypeException is thrown by the Api class.
    When there is an attempt to fetch a property by type and the 
    property is stored under a different type in the tree.
    '''
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message


class JsonException(Error):
    '''The JsonException is thrown by the Api class.
    When there is an error parsing the Json.
    '''
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
